//Interfaccia della classe Mappa, necessaria per aumentare il disaccopiamento tra il package attuale e gli altri
package com.gruppo1.gestionemappa;

import java.io.File;
import java.util.LinkedList;

import org.mapsforge.core.model.LatLong;

import android.content.Context;
import android.os.Handler;

import com.graphhopper.GraphHopper;

public interface IntMappa {

	//Restituisce il percorso in cui si trova la mappa
	public File getAreaFolder();

	//Restituisce il percorso della cartella che contiene la mappa
	public File getMapsFolder();

	//Restituisce il nome della mappa
	public String getMapName();

	//Restituisce la versione della mappa locale
	public int getVersioneMappa();

	//Setta la versione della mappa locale
	public void setVersioneMappa(int versioneMappa);

	//Restituisce la versione dei dati presenti localmente
	public int getVersioneDati();

	//Setta la versione dei dati presenti localmente
	public void setVersioneDati(int versioneDati);

	//Effettua il calcolo del percorso migliore
	public void routing(double fromLat, double fromLon,	LinkedList<LatLong> destinazioni, GraphHopper hopper,
			Handler messageHandler, LatLong destinazionePrecedente,	Context context);

	//Crea il grafo per effettuare il routing
	public void loadGraphStorage(final Context context,	final Handler messageHandler);
}
