/*Classe contenente tutte le informazioni relative alla mappa. All'interno troviamo il nome e la posizione della mappa,
 * si effettua il caricamento della mappa, si crea il grafo per il routing e si gestiscono la versione della mappa e dei dati
*/
package com.gruppo1.gestionemappa;

import java.io.File;
import java.util.LinkedList;

import org.mapsforge.core.model.LatLong;

import com.graphhopper.GraphHopper;
import android.content.Context;
//Classe che implementa l'nterfaccia IntMappa e contiene tutte le informazioni e i metodi riguardanti la mappa.
import android.graphics.Path;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

public class Mappa implements IntMappa {

	private File mapsFolder;
	private File areaFolder;
	private final String nomeMappa = "map-gh";
	private int versioneMappa;
	private int versioneDati;
	private GraphHopper hopper;
	private boolean greaterOrEqKitkat;

	//Metodo costruttore che crea la cartella per la mappa in base alla versione del SO e preleva i valori in input delle versioni
	public Mappa(int versioneMappa, int versioneDati) {
		greaterOrEqKitkat = Build.VERSION.SDK_INT >= 19;
		if (greaterOrEqKitkat) {
			mapsFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/graphhopper/maps/");
		} else {
			mapsFolder = new File(Environment.getExternalStorageDirectory(), "/graphhopper/maps/");
		}
		if (!mapsFolder.exists()) {
			mapsFolder.mkdirs();
		}
		areaFolder = new File(mapsFolder, nomeMappa);
		this.versioneMappa = versioneMappa;
		this.versioneDati = versioneDati;
	}

	//Effettua il calcolo del percorso migliore
	public void routing(double fromLat, double fromLon,	LinkedList<LatLong> destinazioni, GraphHopper hopper,
			Handler messageHandler, LatLong destinazionePrecedente,	Context context) {
		new RoutingAsyncTask(fromLat, fromLon, destinazioni, hopper, messageHandler, destinazionePrecedente, context)
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	//Restituisce il percorso in cui si trova la mappa
	@Override
	public File getAreaFolder() {
		return areaFolder;
	}

	//Restituisce il percorso della cartella che contiene la mappa
	@Override
	public File getMapsFolder() {
		return mapsFolder;
	}

	//Restituisce il nome della mappa
	@Override
	public String getMapName() {
		return nomeMappa;
	}

	//Restituisce la versione della mappa locale
	@Override
	public int getVersioneMappa() {
		return versioneMappa;
	}

	//Setta la versione della mappa locale
	@Override
	public void setVersioneMappa(int versioneMappa) {
		this.versioneMappa = versioneMappa;
	}

	//Restituisce la versione dei dati presenti localmente
	@Override
	public int getVersioneDati() {
		return versioneDati;
	}

	//Setta la versione dei dati presenti localmente
	@Override
	public void setVersioneDati(int versioneDati) {
		this.versioneDati = versioneDati;
	}
	
	//Metodo utilizzato per instanziare la variabile del grafo all'interno del task asincrono
	private GraphHopper createMyGraphHopper(Context context) {
		GraphHopper tmpHopp = new MyGraphHopper(context).forMobile();
		tmpHopp.setCHEnable(false);
		tmpHopp.load(new File(mapsFolder, "map").getAbsolutePath());

		return tmpHopp;
	}

	//Metodo per la creazione del grafo che viene fatta in un task asincrono. Si effettua l'override della classe GHAsynkTask
	public void loadGraphStorage(final Context context,	final Handler messageHandler) {
		new GHAsyncTask<Void, Void, Path>() {
			@Override
			protected Path saveDoInBackground(Void... v) throws Exception {
				hopper = createMyGraphHopper(context);
				return null;
			}

			@Override
			protected void onPostExecute(Path o) {
				int msgWhat = 4;
				if (hasError()) {
					msgWhat = -3;
				}
				final Message resultMessage = messageHandler.obtainMessage(msgWhat, hopper);
				messageHandler.sendMessage(resultMessage);
			}
		}.execute();
	}
}
