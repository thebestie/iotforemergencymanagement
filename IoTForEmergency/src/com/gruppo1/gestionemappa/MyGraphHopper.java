//Classe per la personalizzazione della libreria GraphHopper, necessaria per la creazione un sistema di pesi personalizzato
package com.gruppo1.gestionemappa;

import android.content.Context;

import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.routing.util.WeightingMap;

public class MyGraphHopper extends GraphHopper {

	private Context context;

	public MyGraphHopper(Context context) {
		super();
		this.context = context;
	}

	//Metodo che viene richiamato automaticamente per l'assegnamento dei pesi agli archi
	@Override
	public Weighting createWeighting(WeightingMap wMap, FlagEncoder encoder) {
		return new MyWeighting(context);
	}

}
