/* Questa classe personalizza l'assegnamento dei pesi agli archi del grafo implementndo la classe Weighting della libreria GraphHopper.
*/
package com.gruppo1.gestionemappa;

import android.app.Activity;
import android.content.Context;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.util.EdgeIteratorState;
import com.gruppo1.iotforemergency.MyApplication;

class MyWeighting implements Weighting 
{
	private Context context;
	private MyApplication applicazione;
	private EdgeIteratorState edgeAttuale;
	private Double pesoAssegnato;
	private final Double PESO_ARCHI_NON_MAPPATI=3.00;
	
	//Metodo costruttore che prende in input il context dell'applicazione necessario per recuperare l'istanza della classe MyApplication
    public MyWeighting(Context context )
    {
    	this.context=context;
    	applicazione= (MyApplication) ((Activity)context).getApplication();
    }

    //Metodo usato nel caso si sceglie come algoritmo di routing A*
    @Override
    public double getMinWeight( double distance )
    {
        return distance;
    }

    //Metodo che assegna il peso a ciascun arco e viene richiamato tante volte quanti sono gli archi
    @Override
    public double calcWeight( EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId )
    {
    	pesoAssegnato=PESO_ARCHI_NON_MAPPATI;
    	int contatoreEdge=applicazione.getContatoreEdge();
    	edgeAttuale=applicazione.getEdgeAttuale();
    	
    	//I primi due edge sono quelli virtuali creati da graphhopper e di default prendono la distanza dal nodo per cui qui li cambio
    	if(contatoreEdge<=2 && applicazione.getPesoFisso(edgeAttuale.getEdge())!=-1.00)
    	{
    		applicazione.setContatoreEdge(contatoreEdge+1);
    		Double pesoTotArco=applicazione.getPesoFisso(edgeAttuale.getEdge())+applicazione.getLos(edgeAttuale.getEdge());
    		pesoAssegnato= pesoTotArco*edgeState.getDistance()/edgeAttuale.getDistance();
    	}else if(contatoreEdge<=2)
    	{
    		applicazione.setContatoreEdge(contatoreEdge+1);
    		pesoAssegnato=PESO_ARCHI_NON_MAPPATI*edgeState.getDistance()/edgeAttuale.getDistance();
    	}
    	else if(applicazione.getPesoFisso(edgeState.getEdge())!=-1.00)
		{
    		pesoAssegnato=applicazione.getPesoFisso(edgeState.getEdge())+applicazione.getLos(edgeState.getEdge());
		}	
    	
    	return pesoAssegnato;
    }

    //Metodo che assegna un nome al meccanismo di creazione dei pesi definito sopra
    @Override
    public String toString()
    {
        return "MyWeighting";
    }
}
