/*Classe per il calcolo del routing che in GraphHopper deve essere fatto in un task asincrono. Per il calcolo del percorso migliore vengono calcolati prima i percorsi 
*verso ciascun punto di raccolta e infine si sceglie quello con il peso minore. Inoltre in caso di percorsi con lo stesso peso viene selezionato quello verso il punto di raccolta selezionato precedentemente se tra i migliori
*/
package com.gruppo1.gestionemappa;

import java.util.LinkedList;
import java.util.ListIterator;

import org.mapsforge.core.model.LatLong;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.AlgorithmOptions;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.StopWatch;
import com.gruppo1.iotforemergency.MyApplication;
import com.gruppo1.model.DbAdapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class RoutingAsyncTask extends AsyncTask<Void, Void, GHResponse> {

	private double fromLat, fromLon;
	private LinkedList<LatLong> destinazioni;
	private GraphHopper hopper;
	private Handler messageHandler;
	private LatLong destinazionePrecedente;
	private Context context;
	private MyApplication applicazione;

	//Costruttore: oltre a prelevare il valore dei vari parametri richiama il costruttore della classe padre
	public RoutingAsyncTask(double fromLat, double fromLon,
			LinkedList<LatLong> destinazioni, GraphHopper hopper,
			Handler messageHandler, LatLong destinazionePrecedente, Context context) {
		super();
		this.fromLat = fromLat;
		this.fromLon = fromLon;
		this.destinazioni = destinazioni;
		this.hopper = hopper;
		this.messageHandler = messageHandler;
		this.destinazionePrecedente=destinazionePrecedente;
		this.context=context;
    	applicazione= (MyApplication) ((Activity)context).getApplication();
	}

	//Metodo standard di Android per l'esecuzione di un task parallelo. Qui vengono calcolati i percorsi e viene restituito il migliore (quello con il peso minore)
	@Override
	protected GHResponse doInBackground(Void... v) {
		GHResponse[] resp = new GHResponse[destinazioni.size()]; //Vettore per salvare i percorsi calcolati verso ciascun punto di raccolta
		Double min = Double.MAX_VALUE;
		int rispostaOK = 0, i = 0, vecchioIndice=-1;
		GHRequest req;
		LatLong current, end;
		
		//Setto l'id dell'edge attuale sulla classe myApplication perchè verrà usato dalla classe MyWeighting
    	LocationIndex locationIndex = hopper.getLocationIndex();
    	QueryResult queryPosizioneAttuale = locationIndex.findClosest(
					fromLat,fromLon, EdgeFilter.ALL_EDGES);
    	EdgeIteratorState edgeAttuale = queryPosizioneAttuale.getClosestEdge();
		applicazione.setEdgeAttuale(edgeAttuale);

		ListIterator<LatLong> iter = destinazioni.listIterator();
		
		//Calcolo il percorso per ciascun punto di raccolta 
		while (iter.hasNext()) {
			applicazione.setContatoreEdge(1);
			current = iter.next();
			req = new GHRequest(fromLat, fromLon, current.latitude,
					current.longitude)
					.setAlgorithm(AlgorithmOptions.DIJKSTRA_BI);
			req.getHints().put("instructions", "true");
			req.setWeighting("MyWeighting");
			req.setVehicle("foot");

			resp[i] = hopper.route(req);
			
			//Prelevo le coordinate del punto di arrivo che è leggermente diverso dalle coordinate del punto di raccolta poichè GraphHopper calcola il punto più vicino sulla strada
			end = new LatLong(resp[i].getPoints().getLatitude(
					resp[i].getPoints().getSize() - 1), resp[i]
					.getPoints().getLongitude(
							resp[i].getPoints().getSize() - 1));
			
			//Vedo a quale indice corrisponde la destinazione selezionata precedentemente. Serve in caso di percorsi con peso uguale a selezionare quello corrispondente al punto di raccolta precedentemente selezionato
			if(vecchioIndice==-1 && destinazionePrecedente!=null && end.latitude==destinazionePrecedente.latitude && end.longitude==destinazionePrecedente.longitude )
			{
				vecchioIndice=i;
			}
			i++;
		}

		//Cerco il percorso a peso minore
		for (i = 0; i < destinazioni.size(); i++) {
			if (resp[i].getRouteWeight() < min) {
				min = resp[i].getRouteWeight();
				rispostaOK = i;
			}
		}
		
		//Se il peso del percorso migliore è uguale al peso del percorso verso il punto di raccolta selezionato precedentemente, allora prendi quello.
		if(vecchioIndice!=-1 && resp[rispostaOK].getRouteWeight()==resp[vecchioIndice].getRouteWeight())
		{
			rispostaOK=vecchioIndice;
		}
		
		return resp[rispostaOK];
	}

	//Metodo android che viene richiamato al termine del doInBackground. Vinee restitutito il percorso e -2 se non ci sono errori, altrimenti viene restituito -1
	@Override
	protected void onPostExecute(GHResponse resp) {
		int msgWhat=1;
		if (resp.hasErrors()) {
			msgWhat=-2;
		}
		
		final Message resultMessage = messageHandler.obtainMessage(msgWhat, resp);
		messageHandler.sendMessage(resultMessage);
	}

}
