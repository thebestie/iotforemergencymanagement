/* Classe per la decompressione di un archivio */
package com.gruppo1.gestioneserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.util.Log;

public class Decompress {
	private String zipFile;
	private String location;
	
	public Decompress(String zipFile, String location) {
	    this.zipFile = zipFile;
	    this.location = location;
	
	    dirChecker("");
	}
	
	public Boolean unzip() {
		Boolean state = false;
	    try  {
	        FileInputStream fin = new FileInputStream(zipFile);
	        ZipInputStream zin = new ZipInputStream(fin);
	
	        byte b[] = new byte[1024];
	
	        ZipEntry ze = null;
	        while ((ze = zin.getNextEntry()) != null) {
	            Log.d("IOT", "Estraendo " + ze.getName());
	
	            if(ze.isDirectory()) {
	                dirChecker(ze.getName());
	            } else {
	                FileOutputStream fout = new FileOutputStream(location + ze.getName());
	                BufferedInputStream in = new BufferedInputStream(zin);
	                BufferedOutputStream out = new BufferedOutputStream(fout);
	
	                int n;
	                while ((n = in.read(b,0,1024)) >= 0) {
	                    out.write(b,0,n);
	                }
	
	                zin.closeEntry();
	                out.close();
	            }
	        }
	        zin.close();
	        state = true;
	    } catch(Exception e) {
	        Log.d("IOT", "Errore durante l'estrazione" + e.toString());
	    }
	    return state;
	}
	
	private void dirChecker(String dir) {
	    File f = new File(location + dir);
	    
	    if(!f.isDirectory()) {
	        f.mkdirs();
	    }
	}
}