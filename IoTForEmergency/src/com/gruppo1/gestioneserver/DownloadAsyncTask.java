// Task asincrono GET per il download di un file

package com.gruppo1.gestioneserver;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Base64;
import android.util.Log;

class DownloadAsyncTask extends AsyncTask<String,Integer,String>
{
	
	private Handler messageHandler;
	private Message progressValue;
	private int msgWhat;
	private Boolean error;
	private SSLContext mySSLContext;
	private String credenziali = "";
	
	/**
	 * constructor
	 * @return 
	 */
	public DownloadAsyncTask(Handler messageHandler, SSLContext mySSLContext, int msgWhat, String... credenziali) {
		this.messageHandler = messageHandler;
		this.msgWhat = msgWhat;
		error = false;
		this.mySSLContext = mySSLContext;
		if ((credenziali.length > 0)) {
			this.credenziali = credenziali[0] + ":" + credenziali[1];
		}
	}
	
	// Metodo che viene chiamato prima del doInBackground
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	
	//Metodo standard di Android per l'esecuzione di un task parallelo. Qui viene inizializzata e aperta una connessione HTTPS
	@Override
	protected String doInBackground(String... params) {
		String result = "";
		String errorMessage = "";
		URL url = null;
		HttpsURLConnection conn = null;
		InputStream inStream = null;
		PrintWriter outStream = null;
		OutputStream fileStream = null;
		int fileLength = 0;
		try {
			url = new URL(params[0]);
		 	conn = (HttpsURLConnection) url.openConnection();
		 // Dico alla connessione di utilizzare il nostro SocketFactory dal nostro SSLContext
		 	conn.setSSLSocketFactory(mySSLContext.getSocketFactory());
 			//Rimpiazzo la hostname verification per ovviare al problema : Hostname 'example.com' was not verified, 
 			//non si pu� lasciare "sicuro" perch� certificati e indirizzi sono tutti fittizi.
 			//quindi bisogna fare un return true alla verifica dell'hostname, questo che rischi comporta?
 			//il problema che porta � che utilizzando ad esempio un host qualsiasi es. example.com, con un attacco dns
 			//se qualcuno cambia l'indirizzo a example.com e ci da un certificato maligno non possiamo saperlo perch� non facciamo
 			//la verifica dell'hostname
 			HostnameVerifier hostnameVerifier = new HostnameVerifier() {
 				@Override
 				public boolean verify(String hostname, SSLSession session) {
 				        return true;
 				}
 			};
 			conn.setHostnameVerifier(hostnameVerifier);
		 	conn.setRequestMethod("GET");
		 	conn.setDoInput(true);
			conn.setReadTimeout(15000);
		    conn.setConnectTimeout(15000);
		    if(!credenziali.isEmpty()) {
		    	credenziali = "Basic "+ Base64.encodeToString(credenziali.getBytes("UTF-8"), Base64.DEFAULT);
		    	conn.setRequestProperty("Authorization", credenziali);
		    }
		 	conn.connect();
		 	Log.d("IOT", "Response code " + conn.getResponseCode());
		 	if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
		 		fileLength = conn.getContentLength();
		 		inStream = conn.getInputStream();
		 		Log.d("IOT", "Dimensione file: " + fileLength);
		 		fileStream = new FileOutputStream(params[1]);
		 		byte data[] = new byte[4096];
	            long total = 0;
	            int count;
	            while ((count = inStream.read(data)) != -1) {
	            	total += count;
	            	 if (fileLength > 0) {
	            		 publishProgress((int) (total * 100 / fileLength));
	            	 }
	            	fileStream.write(data, 0, count);
	            }
	            fileStream.flush();
		 		result = conn.getHeaderField("Versione");
		 	} else if(conn.getHeaderField("IOT") != null) {
		 		errorMessage =  Html.fromHtml(conn.getHeaderField("IOT")).toString();
		 		throw new Exception(conn.getResponseCode() + "-" + errorMessage);
		 	} else {
		 		throw new Exception(Integer.toString(conn.getResponseCode()));
		 	}
		}
		catch(Exception e){
			if(errorMessage.isEmpty()) {
				result = "0-Problema di rete. Riprova pi� tardi";
			} else {
				result = e.getMessage().split("-")[0] + "-" + "Problema di rete. Riprova pi� tardi";
			}
		    Log.d("IOT", "Errore durante il download: " + e.toString());
		    error = true;
		}
		finally {
			try {
				if(fileStream != null) {
					fileStream.close();
				}
				if(inStream != null) {
					inStream.close();
				}
				if(outStream != null) {
					outStream.close();
				}
			}
			catch(Exception e) {
				Log.d("IOT", "Errore durante la chiusura degli stream: " + e.toString());
			}
			
			if (conn != null) {
	                conn.disconnect();
			}
		}
		Log.d("IOT", "Risultato download (versione): " + result);
		return result;
	}
	
	// Metodo che viene eseguito durante il doInBackground per segnalare i progressi
	@Override
	protected void onProgressUpdate(Integer... progress) {
		progressValue = messageHandler.obtainMessage(msgWhat - 1, progress[0]);
		messageHandler.sendMessage(progressValue);
    }

	// Metodo che viene eseguito al termine della doInBackground, utilizzato per inviare il messaggio di conclusione del task
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if(error == true) {
			msgWhat = -1;
		}
		progressValue = messageHandler.obtainMessage(msgWhat, result);
		messageHandler.sendMessage(progressValue);
	}

}
