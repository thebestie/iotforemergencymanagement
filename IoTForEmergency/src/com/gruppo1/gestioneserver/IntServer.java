//Interfaccia della classe Server, necessaria per aumentare il disaccopiamento tra il package attuale e gli altri
package com.gruppo1.gestioneserver;

import javax.net.ssl.SSLContext;

import android.content.Context;
import android.os.Handler;

public interface IntServer {
	
	//Invia al server la versione della mappa presente sul dispositivo e riceve un messaggio true / false se esiste o meno una nuova versione 
public void verificaMappaObsoleta(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int versione, int msgWhat);
	
	//Invia al server la versione dei dati presenti sul dispositivo e riceve un messaggio true / false se esiste o meno una nuova versione
	public void verificaDatiObsoleti(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int versione, int msgWhat);
	
	//Avvia il task asincrono per il download della mappa
	public void downloadMappa(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, String path, int msgWhat);
	
	//Elabora il pacchetto della mappa scaricato dal server e lo installa sul dispositivo
	public Boolean installMappa(String path, int versione, String citta, Context context);
	
	//Avvia il task asincrono per l'acquisizione dei dati
	public void getDati(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, String path, int msgWhat);
	
	//Elabora i dati acquisiti e li installa nel dispositivo
	public int installDati(String result, String citta, Context context);
	
	//Avvia il task asincrono per la registrazione di un utente
	public void registrazione(String nickname, String nome, String cognome, String password, SSLContext mySSLContext, Handler messageHandler);

	//Verifica se � presente o meno una connessione
	public Boolean checkConnection(Context context);
	
	//Avvia il task asincrono per l'acquisizione dei LOS
	public void getLOS(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int msgWhat);
	
	//Avvia il task asincrono per modificare un LOS sul server
	public void changeLOS (String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int msgWhat, Integer idPrec,Integer idSucc);
	
	//Avvia il task asincrono per ricevere la lista delle citt� disponibili
	public void getCittaDisponibili(String nick, String password, Handler messageHandler, SSLContext mySSLContext, int msgWhat);
}


