//Task asincrono per eseguire una chiamata POST verso il server
package com.gruppo1.gestioneserver;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Base64;
import android.util.Log;

class PostAsyncTask extends AsyncTask<String,String,String>
{
	private HashMap<String, String> data = null;// post data
	private Handler messageHandler;
	private SSLContext mySSLContext;
	private Boolean error;
	private String credenziali = "";
	private int msgWhat;

	/**
	 * constructor
	 * 
	 * @return
	 */
	public PostAsyncTask(HashMap<String, String> data,Handler messageHandler, SSLContext mySSLContext,  int msgWhat, String... credenziali) {
		this.data = data;
		this.messageHandler = messageHandler;
		this.mySSLContext = mySSLContext;
		error = false;
		this.msgWhat = msgWhat;
		if ((credenziali.length > 0)) {
			this.credenziali = credenziali[0] + ":" + credenziali[1];
		}
	}

	// Metodo che viene chiamato prima del doInBackground
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}
		
	//Metodo standard di Android per l'esecuzione di un task parallelo. Qui viene inizializzata e aperta una connessione HTTPS
	@Override
	protected String doInBackground(String... params) {

		String result = "";
		String errorMessage = "";
		URL url = null;
		HttpsURLConnection conn = null;
		Scanner inStream = null;
		PrintWriter outStream = null;
		try {			
			url = new URL(params[0]);
			conn = (HttpsURLConnection) url.openConnection();
			// Dico alla connessione di utilizzare il nostro SocketFactory dal nostro SSLContext
			conn.setSSLSocketFactory(mySSLContext.getSocketFactory());
			//Rimpiazzo la hostname verification per ovviare al problema : Hostname 'example.com' was not verified, 
			//non si pu� lasciare "sicuro" perch� certificati e indirizzi sono tutti fittizi.
			//quindi bisogna fare un return true alla verifica dell'hostname, questo che rischi comporta?
			//il problema che porta � che utilizzando ad esempio un host qualsiasi es. example.com, con un attacco dns
			//se qualcuno cambia l'indirizzo a example.com e ci da un certificato maligno non possiamo saperlo perch� non facciamo
			//la verifica dell'hostname
			HostnameVerifier hostnameVerifier = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
				        return true;
				}
			};
			conn.setHostnameVerifier(hostnameVerifier);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setReadTimeout(15000);
		    conn.setConnectTimeout(15000);
		    if(!credenziali.isEmpty()) {
		    	credenziali = "Basic "+ Base64.encodeToString(credenziali.getBytes("UTF-8"), Base64.DEFAULT);
		    	conn.setRequestProperty("Authorization", credenziali);
		    }
		    //Parametri della post
 			Iterator<String> it = data.keySet().iterator();
 			String postParams = "";
 			while (it.hasNext()) {
 				String key = it.next();
 				postParams+= key + "=" + URLEncoder.encode(data.get(key), "UTF-8") + "&";	
 			}
 			postParams = postParams.substring(0, postParams.length()-1);
 			Log.d("IOT", "Parametri della chiamata POST: " + postParams);
 			outStream = new PrintWriter(conn.getOutputStream());
 			outStream.print(postParams);
 			outStream.close();
 			
			conn.connect();
			Log.d("IOT", "Response Code: " + conn.getResponseCode());
			if(conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inStream = new Scanner(conn.getInputStream(), "UTF-8"); 
				while(inStream.hasNextLine()) {
					result+=(inStream.nextLine());
				}
			} else if(conn.getHeaderField("IOT") != null) {
				errorMessage = Html.fromHtml(conn.getHeaderField("IOT")).toString();
		 		throw new Exception(conn.getResponseCode() + "-" + errorMessage);
		 	} else {
		 		throw new Exception(Integer.toString(conn.getResponseCode()));
		 	}
	        Log.d("IOT", "Risultato della POST: " + result);
		} catch (Exception e) {
			Log.d("IOT", "Errore nella chiamta POST: " + e.toString());
			if(errorMessage.isEmpty()) {
				result = "0-Problema di rete. Riprova pi� tardi";
			}
			else {
				result = e.getMessage();
			}
			error = true;
		}
		finally {
			try {
				if(outStream != null) {
					outStream.close();
				}
				if(inStream != null) {
					inStream.close();
				}	
			}
			catch (Exception e) {
				Log.d("IOT", "Errore durante la chiusura degli stream: " + e.toString());
			}
			
			if (conn != null) {
                conn.disconnect();
			}
		}		
		return result;

	}

	// Metodo che viene eseguito al termine della doInBackground, utilizzato per inviare il messaggio di conclusione del task
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		if (error == true) {
			msgWhat = -1;
		}
		final Message resultMessage = messageHandler.obtainMessage(msgWhat, result);
		messageHandler.sendMessage(resultMessage);
	}

}