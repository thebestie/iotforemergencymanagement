// Classe che inizializza le comunicazioni del dispositivo con il server
package com.gruppo1.gestioneserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import javax.net.ssl.SSLContext;

import org.json.JSONArray;
import org.json.JSONObject;
import com.gruppo1.model.DbAdapter;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

public class Server implements IntServer {
	
	private String risposta="";
	private String URL;
	private DbAdapter dbAdapter; 
    private Cursor cursor;
	
    public Server(String ip) {
    	this.URL = ip + ":8181/IoTServer";
    }
    
    //Invia al server la versione della mappa presente sul dispositivo e riceve un messaggio true / false se esiste o meno una nuova versione
	@Override
	public void verificaMappaObsoleta(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int versione, int msgWhat) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("https")
			.encodedAuthority(URL)
		    .appendPath("citta")
		    .appendPath("mappaObsoleta")
		    .appendQueryParameter("nome", citta)
		    .appendQueryParameter("versione", Integer.toString(versione));
		new GetAsyncTask(messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString());
	}
	
	//Invia al server la versione dei dati presenti sul dispositivo e riceve un messaggio true / false se esiste o meno una nuova versione
	@Override
	public void verificaDatiObsoleti(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int versione, int msgWhat) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("https")
			.encodedAuthority(URL)
		    .appendPath("citta")
		    .appendPath("pesiObsoleti")
		    .appendQueryParameter("nome", citta)
		    .appendQueryParameter("versione", Integer.toString(versione));
		new GetAsyncTask(messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString());
	}
	
	//Avvia il task asincrono per il download della mappa
	@Override
	public void downloadMappa (String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, String path, int msgWhat) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("https")
		    .encodedAuthority(URL)
		    .appendPath("mappe")
		    .appendQueryParameter("citta", citta);
		new DownloadAsyncTask(messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString(), path + "/map-gh.zip");
	}
	
	//Elabora il pacchetto della mappa scaricato dal server e lo installa sul dispositivo
	@Override
	public Boolean installMappa (String path, int versione, String citta, Context context) {
		Boolean stato = false;
		File oldMap = null;
		Decompress estrai = null;
		Boolean result = false;
		BufferedReader br = null;
		String sCurrentLine = "";
		String raccolta = "";
		JSONObject jsonObj = null;
		JSONArray puntiRaccolta = null;
		JSONObject puntoRaccolta = null;
		File zipFile = null;
		try {
			//Rinomino la vecchia mappa per installare la nuova
			oldMap=new File(path + "/map-gh/");
			oldMap.renameTo(new File(path + "/old_map-gh/"));
			//Estraggo lo zip della nuova mappa
			estrai = new Decompress(path + "/map-gh.zip", path + "/map-gh/");
			result = estrai.unzip();
			//Se l'operazione non va a buon fine riporto la situazione allo stato precedente
			if(result == false) {
				File newMap = new File(path + "/map-gh/");
				DeleteDir.deleteDirectory(newMap);
				oldMap.renameTo(new File(path + "/map-gh/"));
			} else {//Se l'operazione � andata a buon fine cancello i vecchi file e aggiorno nel database il valore del timestamp
				oldMap = new File(path + "/old_map-gh/");
				DeleteDir.deleteDirectory(oldMap);
				dbAdapter = new DbAdapter(context);
        	    dbAdapter.open();	    
        	    dbAdapter.updateVersioneMappa(versione);
        	    dbAdapter.deleteRaccolta();
    			br = new BufferedReader(new FileReader(path + "/map-gh/raccolta.json"));
    			while ((sCurrentLine = br.readLine()) != null) {
    				raccolta += sCurrentLine;
    			}
    			br.close();
    			jsonObj = new JSONObject(raccolta);
				puntiRaccolta = jsonObj.getJSONArray("raccolta");
				for(int i = 0; i < puntiRaccolta.length(); i++) {
					puntoRaccolta = puntiRaccolta.getJSONObject(i);
					dbAdapter.insertRaccolta(puntoRaccolta.getInt("id"), citta, puntoRaccolta.getDouble("lat"), puntoRaccolta.getDouble("lon"));
				}
        	    dbAdapter.close();
			}
			zipFile = new File(path + "/map-gh.zip");
			zipFile.delete();
			stato = true;
		} catch (Exception e) {
			Log.d("IOT", "Errore durante l'installazione della mappa" + e.toString());
		}
		return stato;
	}
	
	//Avvia il task asincrono per l'acquisizione dei dati
	@Override
	public void getDati(String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, String path, int msgWhat) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("https")
		    .encodedAuthority(URL)
		    .appendPath("pesi")
		    .appendQueryParameter("citta", citta);
		new GetAsyncTask(messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString());
	}
	
	//Elabora i dati acquisiti e li installa nel dispositivo
	@Override
	public int installDati(String pesiJson, String citta, Context context) {
		int stato = 0;
		int versione = 0;
		if(!pesiJson.isEmpty()) {
			try {
				dbAdapter = new DbAdapter(context);
        	    dbAdapter.open();  
        	    dbAdapter.deletePesi();
				JSONObject jsonObj = new JSONObject(pesiJson);
				versione = jsonObj.getInt("versione");
				dbAdapter.updateVersioneDati(versione);
				JSONArray pesi = jsonObj.getJSONArray("pesi");
				JSONObject peso = null;
				for(int i = 0; i < pesi.length(); i++) {
					peso = pesi.getJSONObject(i);
					dbAdapter.insertPeso(peso.getInt("id"), citta, peso.getDouble("k"));
				}
				dbAdapter.close();
				stato = versione;
			} catch(Exception e) {
				Log.d("IOT", "Errore durante l'installazione dei pesi: " + e.toString());
			}
		}
		return stato;
	}
	
	//Avvia il task asincrono per la registrazione di un utente
	@Override
	public void registrazione(String nickname, String nome, String cognome, String password, SSLContext mySSLContext, Handler messageHandler) {
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("nick", nickname);
		data.put("nome", nome);
		data.put("cognome", cognome);
		data.put("pass", password);
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("https")
		    .encodedAuthority(URL)
		    .appendPath("utenti");
		new PostAsyncTask(data, messageHandler, mySSLContext, 0).execute(builder.build().toString());
	}
	
	//Verifica se � presente o meno una connessione
	@Override
	public Boolean checkConnection(Context context) {
	    ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	    return networkInfo != null && networkInfo.isConnected();
	}
	
	//Avvia il task asincrono per l'acquisizione dei LOS
	@Override
	public void getLOS (String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int msgWhat) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
            .encodedAuthority(URL)
            .appendPath("pesi")
            .appendPath("los")
            .appendQueryParameter("citta", citta);
        new GetAsyncTask(messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString());
    }
    
	//Avvia il task asincrono per modificare un LOS sul server
    @Override
	public void changeLOS (String nick, String password, Handler messageHandler, SSLContext mySSLContext, String citta, int msgWhat, Integer idPrec,Integer idSucc) {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("citta", citta);
        data.put("idPrec", idPrec.toString());
        data.put("idSucc", idSucc.toString());
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
            .encodedAuthority(URL)
            .appendPath("pesi");
        Log.v("IOT","ID Arco Precedente: "+ idPrec + " ID Arco Successivo: "+ idSucc);
        new PutAsyncTask(data, messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString());
    }
	
    //Avvia il task asincrono per ricevere la lista delle citt� disponibili
	@Override
	public void getCittaDisponibili(String nick, String password, Handler messageHandler, SSLContext mySSLContext, int msgWhat) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("https")
		    .encodedAuthority(URL)
		    .appendPath("citta");
		new GetAsyncTask(messageHandler, mySSLContext, msgWhat, nick, password).execute(builder.build().toString());
	}
}
