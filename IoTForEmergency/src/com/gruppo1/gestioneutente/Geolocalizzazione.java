/*Classe che si occupa della gestione del servizio per la geolocalizzazione dell'utente.
 * La geolocalizzazione viene fatta  con il solo gps o con la combinazione di gps e rete a seconda 
 * del parametro usaRete. Tale distinzione si � resa necessaria per il routing durante lo spostamento dell'utente
 * in modo da avere una posizione pi� precisa dell'utente forzando l'uso dei satelliti gps.
 */
package com.gruppo1.gestioneutente;

import org.mapsforge.core.model.LatLong;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

public class Geolocalizzazione extends Service implements LocationListener, IntGeolocalizzazione {
	private final Context mContext;
	private Handler messageHandler;
	private Boolean usaRete;

    // Flag per lo stato del GPS
    private boolean isGPSEnabled = false;
    private boolean canGetLocation = false;

    // Flag per lo stato della rete
    private boolean isNetworkEnabled = false;

    private Location location; 
    private double latitude; 
    private double longitude; 

    // La distanza minima per comunicare un aggiornamento in metri
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 1 meters

    // Il tempo minimo per comunicare un update in millisecondi
    private static final long MIN_TIME_BW_UPDATES = 1000 * 2 * 1; // 1 minute=1000 * 60 * 1

    protected LocationManager locationManager;

    //Metodo costruttore che presi i parametri richiama il metodo getLocation per avviare la geolocalizzazione
    public Geolocalizzazione(Context context, Handler messageHandler, Boolean usaRete) {
        this.mContext = context;
        this.messageHandler=messageHandler;
        this.usaRete=usaRete;
        getLocation();
    }

    /*Metodo per l'avvio della geolocalizzazione. Inoltre effettua il controllo che l'utente abbia abilitato
     * il rilevamento della posizione sul cellulare altrimenti viene visualizzato un alert di notifica
     */
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // Prelevo lo stato del GPS
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            
            //Se usaRete � true alora controllo anche la rete, altrimenti no
            if(usaRete){
            // Prelevo lo stato della rete
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            }else{
            isNetworkEnabled=false;
            }

            if (!isGPSEnabled && !isNetworkEnabled) {
                // Non � abilitato alcun servizio
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("IOT", "Rete attiva");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // Se � abilitato il GPS, preleva latitudine e longitudine usando il GPS
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("IOT", "GPS Abilitato");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    //Interrompe l'uso del GPS
   
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(Geolocalizzazione.this);
        }
    }


    //Metodo per prelevare la latitudine
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        return latitude;
    }


    //Metodo per prelevare la longitudine
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        return longitude;
    }

    //Metodo per controllare che il gps/Rete sia abilitato
    public boolean canGetLocation() {
        return this.canGetLocation;
    }


    /* Funzione per mostrare l'alert di notifica in caso di servizio di geolocalizzazione disabilitato.
     * Premendo il pulsante Settings verr� mostrata la pagna di impostazioni
     */
    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setto il titolo dell'alert
        alertDialog.setTitle("Errore GPS!");

        // Setto il messaggio
        alertDialog.setMessage("Il rilevamento della posizione non � abilitato. Vuoi attivarlo?");

        // Azione da eseguire premendo sul pulsante Settings
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // Azione da eseguire premendo il pulsante cancella
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });

        // Mostra l'alert
        alertDialog.show();
    }

    /*Metodo richiamato quando � pasato il tempo minimo impostato in precedenza o l'utente si
     * � spostato della distanza prevista prima
     */
    @Override
    public void onLocationChanged(Location location) {
    		LatLong posizione = new LatLong(location.getLatitude(),location.getLongitude());
    		final Message resultMessage = messageHandler.obtainMessage(0, posizione);
    		messageHandler.sendMessage(resultMessage);
    }


    @Override
    public void onProviderDisabled(String provider) {
    }


    @Override
    public void onProviderEnabled(String provider) {
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
   
}
