//Interfaccia della classe Geolocalizzazione per aumentare il disaccopiiamento
package com.gruppo1.gestioneutente;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

public interface IntGeolocalizzazione {
		
		//Metodo per avviare la geolocalizzazione
	    public Location getLocation();
	    
	    //Metodo per interrompere l'uso del GPS
	    public void stopUsingGPS();
	    
	    public double getLatitude();
	    
	    public double getLongitude();
	    
	    //Metodo per controllare che l'utente abbia abilitato il rilevamento della posizione sul telefono
	    public boolean canGetLocation();
	    
	    //Metodo per mostrare l'alert di avviso
	    public void showSettingsAlert();
	    
	    public void onLocationChanged(Location location);
	    
	    public void onProviderDisabled(String provider);
	    
	    public void onProviderEnabled(String provider);
	    
	    public void onStatusChanged(String provider, int status, Bundle extras);
	    
	    public IBinder onBind(Intent arg0);

}
