/*
 *	Interfaccia della classe utente 
 */
package com.gruppo1.gestioneutente;

import android.content.Context;
import android.os.Handler;

public interface IntUtente {

	public String getUsername();

	public void setUsername(String username);

	public String getPassword();

	public void setPassword(String password);
	
	public int getLastUpdate();
	
	public void setLastUpdate(int timeStamp);
	
	public String getCitta();

	public void setCitta(String citta);
	
	public int getModeLocalizzazione();

	public void setModeLocalizzazione(int modeLocalizzazione);
}
