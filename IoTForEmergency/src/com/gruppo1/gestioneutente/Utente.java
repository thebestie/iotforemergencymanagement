//Classe per la gestione di tutti i dati relativi all'utente
package com.gruppo1.gestioneutente;

import com.gruppo1.iotforemergency.MappaActivity;

import android.content.Context;
import android.os.Handler;

public class Utente implements IntUtente {
	
	private String username;
	private String password;
	private int lastUpdate;
	private String citta;	
	private int modeLocalizzazione; //Dice se la scelta della citt� � automatica o manuale

	//Costruttore
	public Utente(String username, String password, String citta, int lastUpdate, int modeLocalizzazione) {
		super();
		this.username = username;
		this.password = password;
		this.citta=citta;
		this.lastUpdate = lastUpdate;
		this.modeLocalizzazione = modeLocalizzazione;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public int getLastUpdate () {
		return lastUpdate;
	}
	
	@Override
	public void setLastUpdate(int lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	@Override
	public String getCitta() {
		return citta;
	}

	@Override
	public void setCitta(String citta) {
		this.citta = citta;
	}
	
	@Override
	public int getModeLocalizzazione() {
		return modeLocalizzazione;
	}

	@Override
	public void setModeLocalizzazione(int modeLocalizzazione) {
		this.modeLocalizzazione = modeLocalizzazione;
	}
}
