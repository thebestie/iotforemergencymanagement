// Classe di tipo Activity, quindi con relativa interfaccia grafica che mostra il progresso del download dei dati.
package com.gruppo1.iotforemergency;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.mapsforge.core.model.LatLong;

import com.gruppo1.gestionemappa.IntMappa;
import com.gruppo1.gestioneserver.IntServer;
import com.gruppo1.gestioneutente.Geolocalizzazione;
import com.gruppo1.gestioneutente.IntGeolocalizzazione;
import com.gruppo1.gestioneutente.IntUtente;
import com.gruppo1.model.DbAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DownloadActivity extends AppCompatActivity {

	private static final int SETTING_REQUEST = 0;
	private IntServer myServer;
	private IntUtente myUtente;
	private IntMappa myMappa;
	private IntGeolocalizzazione gps;
	private MyApplication applicazione;
	private Context context;
	private AlertDialog.Builder alertDialogBuilder;
	private AlertDialog alertDialog;
	private ProgressBar downloadMapProgress;
	private ImageView checkedImage;
	private TextView progressText;
	private Geocoder geocoder;
	private LatLong posizione;
	private String citta;
	private int datiInstallati;
	private Boolean mappaInstallata;
	private int progressMapValue;
	private Boolean mappaObsoleta;
	private Boolean datiObsoleti;
	private Boolean stop;
	private DbAdapter dbAdapter;
	private Boolean cityFound;
	
	private Handler messageHandler = new Handler() {
		/*
		 * Case -1 : Dialog Errore
		 * Case 0 : Risultato asyncTask per la posizione
		 * Case 1 : Risultato asyncTask per mappa obsoleta
		 * Case 2 : Risultato asyncTask per dati obsoleti e avvio del download mappa o dati
		 * Case 3 : Aggiornamento grafico progressi mappa
		 * Case 4 : Conclusione download mappa, installazione mappa e avvio download dati
		 * Case 5 : Conclusione download dati, installazione dati, avvio download Punti di raccolta
		 * 
		 * Per le chiamate get e download bisogna impostare il msgWhat (l'ultimo parametro) al valore del case in cui si trova
		 * il codice per la gestione della risposta dell'asyncTask (per la mappa 4).
		 * Attenzione: per i donwload il case precedente al case di gestione della risposta deve essere necessariamente il case
		 * di gesione dell'aggiornamento della progress bar.
		*/

        @Override
        public void handleMessage(Message msg) {
        	switch(msg.what) {
        	case -1 :         
        		if((cityFound == false) && (msg.obj.toString().split("-")[0].compareTo("404") == 0)) {
        			missingCity();
        		} else {
        			popUp("Errore!", msg.obj.toString().split("-")[1]);
        		}
        		break;
        	case 0 :
        		posizione = (LatLong) msg.obj;
        		geocoder = new Geocoder(DownloadActivity.this, Locale.getDefault());
	    		List<Address> addresses=null;
	            try {
	            	addresses = geocoder.getFromLocation(posizione.latitude, posizione.longitude, 1);
	            	citta = addresses.get(0).getLocality();	 
		            myUtente.setCitta(citta);
		            dbAdapter.open();	    
	        	    dbAdapter.updateCitta(citta, myUtente.getUsername());
	        	    dbAdapter.close();
		            gps.stopUsingGPS();
		            Log.d("IOT", "Citt� trovata: " + citta);
		            progressText.setText("Controllo la versione della mappa");
		            myServer.verificaMappaObsoleta(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), myUtente.getCitta(), myMappa.getVersioneMappa(),  1);
	        		
	            } catch (IOException e) {
	                Log.d("IOT", "Errore nel ricavo della citt� dalla posizione" + e.toString());
	                popUp("Errore!", "Citt� non trovata. Assicurarsi di avere una connessione ad internet attiva.");
	            }
	            break;
        	case 1 :
        		cityFound = true;
        		mappaObsoleta = Boolean.valueOf((String) msg.obj);
        		progressText.setText("Controllo la versione dei dati");
        		myServer.verificaDatiObsoleti(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), myUtente.getCitta(), myMappa.getVersioneDati(), 2);
        		break;
        	case 2 :
        		datiObsoleti = Boolean.valueOf((String) msg.obj);
        		if(mappaObsoleta == true) {
        			progressText.setText("Eseguo il download della mappa");
        			myServer.downloadMappa(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), myUtente.getCitta(), myMappa.getMapsFolder().toString(), 4);
        		} else if (datiObsoleti == true) {
        			progressText.setText("Eseguo il download dei dati");
        			myServer.getDati(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), myUtente.getCitta(),  myMappa.getMapsFolder().toString(), 5);
        		}
        		else {
        			closeActivity(RESULT_OK);
        		}
        		break;
        	case 3 :
        		progressMapValue = (Integer) msg.obj;
        		downloadMapProgress = (ProgressBar) findViewById(R.id.downloadMapBar);
        		downloadMapProgress.setIndeterminate(false);
                downloadMapProgress.setProgress(progressMapValue);
        		break;
        	case 4 :
        		progressText.setText("Installo la mappa");
        		mappaInstallata = myServer.installMappa(myMappa.getMapsFolder().toString(), Integer.parseInt((String) msg.obj), myUtente.getCitta(), context);
        		if(mappaInstallata == true) {
        			myMappa.setVersioneMappa(Integer.parseInt((String) msg.obj));
	        		if(datiObsoleti == true) {
	        			progressText.setText("Eseguo il download dei dati");
	        			myServer.getDati(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), myUtente.getCitta(), myMappa.getMapsFolder().toString(), 5);
	        		}
	        		else {
	        			closeActivity(RESULT_OK);
	        		}
        		}
        		else {
        			popUp("Errore", "Problema durante l'installazione della mappa, riprova pi� tardi");
        		}
        		break;
        	case 5 :
        		checkedImage.setVisibility(View.VISIBLE);
        		progressText.setText("Installo i dati");
        		datiInstallati = myServer.installDati((String) msg.obj, myUtente.getCitta(), context);
        		if(datiInstallati == 0) {
        			popUp("Errore", "Problema durante l'installazione dei dati, riprova pi� tardi");
        		}
        		else {
        			myMappa.setVersioneDati(datiInstallati);
        			closeActivity(RESULT_OK);
        		}	
        		break;
    		}
        }
    };
	
    // Metodo eseguito alla creazione dell'activity, inizializza i dati dell'activity
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_download);
		
		context = this;
		downloadMapProgress = (ProgressBar) findViewById(R.id.downloadMapBar);
		applicazione = (MyApplication) this.getApplication();
		myServer = applicazione.getMyServer();
		myUtente = applicazione.getMyUtente();
		myMappa = applicazione.getMyMappa();
		
		Log.d("IOT", "Versione mappa: " + myMappa.getVersioneMappa() + " Versione dati: " + myMappa.getVersioneDati());
		
		dbAdapter = new DbAdapter(context);
		alertDialogBuilder = new AlertDialog.Builder(context);
		progressText = (TextView) findViewById(R.id.textProgress);
		checkedImage = (ImageView) findViewById(R.id.checkedImage);
		
		stop = false;
	}
	
	// Metodo eseguito dopo che l'activity � stata messa in primo piano, avvia il processo di download cercando la posizione dell'utente
	@Override
	protected void onResume() {
		super.onResume();
		if(stop == false) {
			if(myUtente.getModeLocalizzazione() == 0) {
				cityFound = false;
				progressText.setText("Cerco la tua posizione");
				gps = new Geolocalizzazione(DownloadActivity.this, messageHandler, true);
				if(!gps.canGetLocation()) {
					gps.showSettingsAlert();
				}
			}
			else {
				cityFound = true;
				progressText.setText("Controllo la versione della mappa");
				myServer.verificaMappaObsoleta(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), myUtente.getCitta(), myMappa.getVersioneMappa(),  1);
			}
		} else {
			popUp("Errore!", "Non hai selezionato una citt�. L'applicazione verr� chiusa");
		}
	}
	
	// Metodo per visualizzare un pop up con un particolare messaggio e chiude la activity
	private void popUp(String titolo, String corpo){
		alertDialogBuilder
		.setTitle(titolo)
		.setMessage(corpo)
		.setCancelable(false)
		.setPositiveButton("Ok",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					closeActivity(RESULT_CANCELED);
				}
			});
         alertDialog = alertDialogBuilder.create();

         alertDialog.show();
	}	
	
	// Metodo per visualizare il pop up che avvia la SettingActivity
	private void missingCity(){
		alertDialogBuilder
		.setTitle("Errore!")
		.setMessage("La tua citt� non � presente sui nostri sistemi. Nella prossima schermata verranno visualizzate le citt� disponibili")
		.setCancelable(false)
		.setPositiveButton("Ok",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					Intent settingActivity = new Intent(DownloadActivity.this, SettingActivity.class);
					startActivityForResult(settingActivity, SETTING_REQUEST);
				}
			});
         alertDialog = alertDialogBuilder.create();

         alertDialog.show();
	}
	
	// Metodo che viene eseguito quando un altra activity viene chiusa e lasciando il risultato della sua esecuzione
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_CANCELED) {
			stop = true;
		}
		else {
			stop = false;
		}
	}
	
	// Metodo per chiudere l'activity in maniera positiva e salvare nel db la data dell'ultimo aggiornamento fatto
	private void closeActivity(int result) {
		String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
		myUtente.setLastUpdate(Integer.parseInt(today));
		Log.d("IOT", "Data di oggi: " + Integer.parseInt(today));
		Log.d("IOT", "Data di oggi: " + myUtente.getLastUpdate());
	    dbAdapter.open();	    
	    dbAdapter.updateLastUpdate(myUtente.getUsername(), Integer.parseInt(today));
	    dbAdapter.close();
		Intent resultIntent = new Intent();
		setResult(result, resultIntent);
		finish();
	}
}
