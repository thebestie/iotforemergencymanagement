/* Activity per la gestione delle altre activity
 * 3 casi principali nell'onCreate:
 * 1) primo avvio
 * 2) registrato ma niente mappa o dati
 * 3) avvio normale
 * 
*/
package com.gruppo1.iotforemergency;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import com.gruppo1.gestionemappa.IntMappa;
import com.gruppo1.gestionemappa.Mappa;
import com.gruppo1.gestioneserver.IntServer;
import com.gruppo1.gestioneserver.Server;
import com.gruppo1.gestioneutente.IntUtente;
import com.gruppo1.gestioneutente.Utente;
import com.gruppo1.model.DbAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
//All'avvio controllo che ci siano i dati dell'utente altrimenti � primo avvio
import android.util.Log;

public class MainActivity extends AppCompatActivity {
	
	private static final int REGISTRATION_REQUEST = 1;
	private static final int DOWNLOAD_REQUEST = 2;
	private static final int MENU_REQUEST = 3;
	private DbAdapter dbAdapter; 
    private Cursor cursor;
    private final Context context = this;
    private IntUtente myUtente;
	private IntServer myServer;
	private IntMappa myMappa;
	private MyApplication applicazione;

	// Inizializza i dati per l'applicaizone e verifica lo stato in cui l'applicazione si trova
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		applicazione= (MyApplication) this.getApplication();
		
		//File di configurazione per sapere l'ip del server
		BufferedReader br;
		String ip = "";
		try {
			br = new BufferedReader(new FileReader(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/config.txt"));
			ip = br.readLine();
		} catch (IOException e) {
			Log.d("IOT", "Errore nella lettura del file di configurazione: " + e.toString());
		}
		
		myServer = new Server(ip);
		applicazione.setMyServer(myServer);
		
		myMappa = new Mappa(0,0);
		applicazione.setMyMappa(myMappa);

		try {
			//Creo un certificato in android a partire dal file certificato preso dal server (per ora installato in asset ma 
			//si potrebbe anche scaricare dal server direttamente come file, magari con una conessione http, cos� evitiamo anche
			//il passaggio della variabile context (anche se questo passaggi si pu� anche saltare usando la classe application
			//che abbiamo fatto
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			InputStream caInput = new BufferedInputStream(context.getAssets().open("server.crt"));
			Certificate cert;
			try {
			    cert = cf.generateCertificate(caInput);
			    Log.d("IOT", "Certificato = " + ((X509Certificate) cert).getSubjectDN());
			} finally {
			    caInput.close();
			}
			
			// Creo un keystore a partire dal certificato
			String keyStoreType = KeyStore.getDefaultType();
			KeyStore keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("iotCert", cert);

			// Creo un TrustManager cos� che android si fidi di quel certificato
			String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);

			// Creo un context SSL che usi il mio TrustManager
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tmf.getTrustManagers(), null);
			applicazione.setMySSLContext(sslContext);			
		} catch(Exception e) {
			Log.d("IOT", "Eccezione durante creazione certificato " + e.toString());
		}
		
		if (controlloPrimoAvvio()) {
			Intent registrazione = new Intent(MainActivity.this, RegistrazioneActivity.class);
			startActivityForResult(registrazione,REGISTRATION_REQUEST);
		} else if(!datiPresenti()) {
			avviaDownload();
		} else {
			avviaSchermataPrincipale();	
		}	
	}
	
	// Controllo sul db se sono stati scaricati in precedenza i dati
	private Boolean datiPresenti() {
		Boolean presenti = false;
		int versioneMappa = 0;
		int versioneDati = 0;
		dbAdapter = new DbAdapter(context);
	    dbAdapter.open();
	    
	    cursor= dbAdapter.selectVersioni();  
	    cursor.moveToFirst();
	    
	    versioneMappa = cursor.getInt(cursor.getColumnIndex(DbAdapter.VERSIONE_KEY_MAPPA));
	    versioneDati = cursor.getInt(cursor.getColumnIndex(DbAdapter.VERSIONE_KEY_DATI));
	   
	    cursor.close();
	    dbAdapter.close();
	    
	    
	    if(versioneMappa != 0 && versioneDati != 0) {
	    	/*
	    	 * Nel caso in cui il download � avvenuto precedentemente con successo ma la cartella della mappa � stata cancellata
	    	 * all'esterno dell'app ad esempio da programmi di pulizia del sistema.
	    	 * Non serve modificare anche il DB perch� la Download Activity controlla il valore nell'oggetto myMappa per decidere
	    	 * se fare o meno il download 
	    	*/
	    	if(myMappa.getAreaFolder().isDirectory()) {
	    		presenti = true;
	    	}else {
	    		versioneMappa = 0; 
	    	}
	    }
	    myMappa.setVersioneDati(versioneDati);
	    myMappa.setVersioneMappa(versioneMappa);
	    Log.d("IOT", "Versione mappa: " + myMappa.getVersioneMappa() + " Versione dati: " + myMappa.getVersioneDati());
		return presenti;
	}
	
	// Viene controllata la presenza di un utente nel db per verificare se � la prima volta che si avvia l'applicazione
	private Boolean controlloPrimoAvvio()
	{
		Boolean primoAvvio=false;
		dbAdapter = new DbAdapter(context);
	    dbAdapter.open();
	    cursor= dbAdapter.selectUtenti();  
	    cursor.moveToFirst();
	    if(cursor.getCount() == 0) {
	    	primoAvvio=true;
	    } else {
            myUtente = new Utente(cursor.getString(cursor.getColumnIndex(DbAdapter.UTENTE_KEY_NICK)),cursor.getString(cursor.getColumnIndex(DbAdapter.UTENTE_KEY_PASSWORD)), cursor.getString(cursor.getColumnIndex(DbAdapter.UTENTE_KEY_CITTA)), cursor.getInt(cursor.getColumnIndex(DbAdapter.UTENTE_KEY_LASTUPDATE)), cursor.getInt(cursor.getColumnIndex(DbAdapter.UTENTE_KEY_MODELOCALIZZAZIONE)));
            applicazione.setMyUtente(myUtente);
        }
	    cursor.close();
	    dbAdapter.close();
		return primoAvvio;
	}
	
	// Quando un'altra activity viene chiusa ne verifica il risultato e agisce di conseguenza
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (resultCode == RESULT_OK) {
			if (requestCode == REGISTRATION_REQUEST) {
				avviaDownload();
			} else if (requestCode == DOWNLOAD_REQUEST) {
				avviaSchermataPrincipale();
			}
			
		}else if (resultCode == RESULT_CANCELED) {
			finish();
		}
	    
	}
	
	//Avvia l'activity per il download
	private void avviaDownload() 
	{
		Intent download= new Intent(MainActivity.this, DownloadActivity.class);
		startActivityForResult(download, DOWNLOAD_REQUEST);
	}
	
	//Avvia l'activity principale che contiene il menu
	private void avviaSchermataPrincipale()
	{
		Intent principale = new Intent(MainActivity.this, MenuActivity.class);
		startActivityForResult(principale,MENU_REQUEST);
	}
	

	
}
