/* Classe di tipo Activity, quindi con relativa interfaccia grafica che mostra la mappa, la posizione dell'utente,  il percorso calcolato, le informazioni aggiuntive sul percorso e il punto di raccolta selezionato.
*/
package com.gruppo1.iotforemergency;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.graphics.Canvas;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Point;
import org.mapsforge.core.model.MapPosition;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.util.AndroidUtil;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.controller.MapViewController;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.overlay.Marker;
import org.mapsforge.map.layer.overlay.Polyline;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.reader.MapDataStore;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.InstructionList;
import com.graphhopper.util.PointList;
import com.graphhopper.util.Translation;
import com.graphhopper.util.TranslationMap;
import com.gruppo1.gestionemappa.IntMappa;
import com.gruppo1.gestioneserver.IntServer;
import com.gruppo1.gestioneutente.Geolocalizzazione;
import com.gruppo1.gestioneutente.IntGeolocalizzazione;
import com.gruppo1.gestioneutente.IntUtente;
import com.gruppo1.model.DbAdapter;

public class MappaActivity extends Activity {

	private MapView mapView;
	private GraphHopper hopper;
	private File areaFolder;
	private TileCache tileCache;
	private volatile boolean prepareInProgress = true;
	private volatile boolean shortestPathRunning = false;
	private IntMappa myMappa;
	private IntServer myServer;
	private IntUtente myUtente;
	private MyApplication applicazione;
	private LatLong posizione;
	private Layers layers;
	private Marker marker;
	private final Context context = this;
	private LinkedList<LatLong> destinazioni;
	private Location posizioneAttuale = new Location("");
	private Location posizionePrecedente = new Location("");
	private Boolean nuovoNodo = false;
	private float distanza;
	private Translation trad;
	private int numeroIstruzione;
	private InstructionList istruzioni;
	private final static Integer RAGGIO = 10;
	private final static Integer TOLLERANZA = 4;
	private LatLong end = null;
	private GHResponse resultMessage = null;
	private EdgeIteratorState edgeAttuale, edgePrecedente;
	private LocationIndex locationIndex;
	private QueryResult queryPosizioneAttuale, queryPosizionePrecedente;
	private int serverDown = 0;
	private Map<Integer, Double> mappaPesiFissi = new HashMap<Integer, Double>();
	private Map<Integer, Double> mappaLOS = new HashMap<Integer, Double>();
	private IntGeolocalizzazione gps;
	private int nodiPolylineModificati = 0;
	private Location n0 = new Location("");
	private Location n1 = new Location("");
	private Location n2 = new Location("");
	private Location virtualPosition = new Location("");
	private DbAdapter dbAdapter;
	private Cursor cursor;
	private AlertDialog.Builder alertDialogBuilder;
	private AlertDialog alertDialog;

	private Handler messageHandler = new Handler() {

		//Il messageHandler � necessario per sincronizzare tra loro i vari task asincroni che in questo caso sono l'interazione con il server e il calcolo del percorso
		@Override
		public void handleMessage(Message msg) {
			/*
			 * case -3: errore caricamento grafo case 
			 * case -2: errore AsynkTask; case
			 * case -1: errore server; case 
			 * case 0: risultato geolocalizzazione; 
			 * case 1: risultato RoutingAsyncTask; 
			 * case 2: risultato AsynkTask Aggiornamento pesi dinamici; 
			 * case 3: risultato invio posizione al server 
			 * case 4: risultato caricamento grafo;
			 */
			switch (msg.what) {
			case -3:
				finishPrepare();
				logUser("Errore nel caricamento della mappa! Riavvia l'applicazione");
				break;

			case -2:
				shortestPathRunning = false;
				logUser("Errore nel calcolo del percorso. Ricalcolo in corso");
				break;

			case -1:
				break;

			case 0:
				posizione = (LatLong) msg.obj;				
				calcolaPercorso(posizione);
				break;

			case 1:
				resultMessage = (GHResponse) msg.obj;
				//logUser("Peso totale: "+ resultMessage.getRouteWeight()); //Decommentare per visualizzare il peso del percorso selezionato
				nodiPolylineModificati = 0;
				shortestPathRunning = false;
				//Ho calcolato un nuovo percorso, azzero la visualizzazione corrente e disegno il nuovo percorso
				layers = mapView.getLayerManager().getLayers();
				while (layers.size() > 1) {
					layers.remove(1);
				}
				marker = createMarker(
						new LatLong(posizioneAttuale.getLatitude(),
								posizioneAttuale.getLongitude()),
						R.drawable.marker_posizione);
				if (marker != null) {
					layers.add(marker);
				}

				end = new LatLong(resultMessage.getPoints().getLatitude(
						resultMessage.getPoints().getSize() - 1), resultMessage
						.getPoints().getLongitude(
								resultMessage.getPoints().getSize() - 1));
				marker = createMarker(end, R.drawable.marker_punto_raccolta);
				if (marker != null) {
					mapView.getLayerManager().getLayers().add(marker);
				}
				mapView.getLayerManager().getLayers()
						.add(createPolyline(resultMessage));
				break;

			case 2:
				// Aggiorno mappaLOS prima di settare i pesi aggiornati
				String pesiJson = (String) msg.obj;
				if (!pesiJson.isEmpty()) {
					try {
						JSONObject jsonObj = new JSONObject(pesiJson);
						JSONArray pesi;
						pesi = jsonObj.getJSONArray("pesi");
						JSONObject peso = null;
						for (int i = 0; i < pesi.length(); i++) {
							peso = pesi.getJSONObject(i);
							mappaLOS.put(peso.getInt("id"),
									peso.getDouble("los"));
						}
						applicazione.setMappaLOS(mappaLOS);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				//Dopo aver aggironato i LOS posso ricalcolare il percorso
				shortestPathRunning = true;
				myMappa.routing(posizioneAttuale.getLatitude(),
						posizioneAttuale.getLongitude(), destinazioni, hopper,
						messageHandler, end, context);
				break;

			case 3:
				Log.v("IOT", "posizione aggiornata");
				break;

			case 4:
				hopper = (GraphHopper) msg.obj;
				finishPrepare();
				break;

			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mappa);

		AndroidGraphicFactory.createInstance(getApplication());

		alertDialogBuilder = new AlertDialog.Builder(context);

		applicazione = (MyApplication) this.getApplication();
		myMappa = applicazione.getMyMappa();
		myServer = applicazione.getMyServer();
		myUtente = applicazione.getMyUtente();

		dbAdapter = new DbAdapter(context);
		dbAdapter.open();

		// Carico i punti di raccolta
		cursor = dbAdapter.selectPuntiRaccoltaByCitta(myUtente.getCitta());
		destinazioni = new LinkedList();
		while (cursor.moveToNext()) {
			destinazioni.add(new LatLong(cursor.getDouble(cursor
					.getColumnIndex(DbAdapter.RACCOLTA_KEY_LAT)), cursor
					.getDouble(cursor
							.getColumnIndex(DbAdapter.RACCOLTA_KEY_LON))));
		}

		// Carico i pesi fissi e popolo il LOS con zero
		cursor = dbAdapter.selectPesiByCitta(myUtente.getCitta());
		while (cursor.moveToNext()) {
			mappaPesiFissi.put(cursor.getInt(cursor
					.getColumnIndex(DbAdapter.PESO_KEY_EDGEID)), cursor
					.getDouble(cursor.getColumnIndex(DbAdapter.PESO_KEY_K)));
			mappaLOS.put(cursor.getInt(cursor
					.getColumnIndex(DbAdapter.PESO_KEY_EDGEID)), 0.00);
		}
		dbAdapter.close();
		cursor.close();
		applicazione.setMappaPesiFissi(mappaPesiFissi);
		applicazione.setMappaLOS(mappaLOS);

		//Le informazioni aggiuntive devono essere in italiano
		TranslationMap SINGLETON = new TranslationMap().doImport();
		trad = SINGLETON.getWithFallBack(Locale.ITALY);

		mapView = new MapView(this);
		mapView.setClickable(true);
		mapView.setBuiltInZoomControls(true);
		mapView.setKeepScreenOn(true);
		mapView.getModel().mapViewPosition.setZoomLevelMax((byte) 22);

		gps = new Geolocalizzazione(MappaActivity.this, messageHandler, false);

		// Controllo che il GPS sia abilitato
		if (!gps.canGetLocation()) {
			gps.showSettingsAlert();
		}
		logUser("Caricamento mappa in corso");
		tileCache = AndroidUtil.createTileCache(this, getClass()
				.getSimpleName(),
				mapView.getModel().displayModel.getTileSize(), 1f, mapView
						.getModel().frameBufferModel.getOverdrawFactor());

		boolean greaterOrEqKitkat = Build.VERSION.SDK_INT >= 19;
		if (greaterOrEqKitkat
				&& !Environment.getExternalStorageState().equals(
						Environment.MEDIA_MOUNTED)) {
			logUser("GraphHopper non pu� essere usato senza uno storage esterno!");
		} else {
			areaFolder = myMappa.getAreaFolder();
			loadMap(areaFolder);
		}
	}

	private void log(String str) {
		Log.i("GH", str);
	}

	private void logUser(String str) {
		log(str);
		Toast.makeText(this, str, Toast.LENGTH_LONG).show();
	}

	private void loadMap(File areaFolder) {
		MapDataStore mapDataStore = new MapFile(new File(areaFolder, "map.map"));

		mapView.getLayerManager().getLayers().clear();

		TileRendererLayer tileRendererLayer = new TileRendererLayer(tileCache,
				mapDataStore, mapView.getModel().mapViewPosition, false, true,
				AndroidGraphicFactory.INSTANCE) {
			
			//Con il tap prolungato sulla mappa seleziono manualmente la posizione e viene ricalcolato il percorso
			@Override
			public boolean onLongPress(LatLong tapLatLong, Point layerXY,
					Point tapXY) {
					calcolaPercorso(tapLatLong);
				return true;
			}
			
			//Con un tap breve sulla mappa visualizzo l'istruzione successiva
			@Override
			public boolean onTap(LatLong tapLatLong, Point layerXY, Point tapXY) {
				if (resultMessage != null) {
					istruzioni = resultMessage.getInstructions();
					
					//Se la strada � dritta l'istruzione di GraphHopper � "Continua su..." per cui devo prelevare l'istruzione zero
					if (numeroIstruzione == 1
							&& istruzioni.get(0).getDistance() > (distanza + RAGGIO)) {
						numeroIstruzione = 0;
					}
					String istruzione = istruzioni.get(numeroIstruzione)
							.getTurnDescription(trad);
					String istruzioneTitleCaped = istruzione.substring(0, 1)
							.toUpperCase() + istruzione.substring(1);
					logUser(istruzioneTitleCaped);
				}

				return true;
			}
		};
		tileRendererLayer.setTextScale(1.5f);
		tileRendererLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);
		mapView.getModel().mapViewPosition.setMapPosition(new MapPosition(
				mapDataStore.boundingBox().getCenterPoint(), (byte) 16));
		mapView.getLayerManager().getLayers().add(tileRendererLayer);

		setContentView(mapView);

		myMappa.loadGraphStorage(context, messageHandler);

	}
	
	/*Metodo per la gestione delle casistiche che possono verificarsi durante lo spostamento.
	 * In particolare il percorso viene ricalcolato solo in prossimit� di un incrocio o quando l'utente cambia strada
	 * mentre le altre volte viene solo aggionata la vista corrente al fine di non appesantire l'applicazione.
	*/
	private Boolean calcolaPercorso(LatLong tapLatLong) {
		Boolean valRestituito;
		//Salvo la posizione precedente dell'utente al fine di capire come si � spostato
		posizionePrecedente.setLatitude(posizioneAttuale.getLatitude());
		posizionePrecedente.setLongitude(posizioneAttuale.getLongitude());
		posizioneAttuale.setLatitude(tapLatLong.latitude);
		posizioneAttuale.setLongitude(tapLatLong.longitude);

		if (!isReady())// Grafo non ancora caricato
		{
			valRestituito = false;
			Log.v("IOT", "Grafo non ancora caricato");

		} else if (shortestPathRunning) {// Routing precedente ancora non
											// terminato
			valRestituito = false;
			Log.v("IOT", "Routing precedente non terminato");
		}

		else if (resultMessage == null)// Prima volta che chiedo il routing, quindi devo calcolare il percorso
		{
			locationIndex = hopper.getLocationIndex();
			queryPosizioneAttuale = locationIndex.findClosest(
					posizioneAttuale.getLatitude(),
					posizioneAttuale.getLongitude(), EdgeFilter.ALL_EDGES);
			edgeAttuale = queryPosizioneAttuale.getClosestEdge();
			nuovoNodo = true;
			numeroIstruzione = 0;
			shortestPathRunning = true;
			valRestituito = true;

			myMappa.routing(posizioneAttuale.getLatitude(),
					posizioneAttuale.getLongitude(), destinazioni, hopper,
					messageHandler, end, context);
			
			//CENTRO LA MAPPA
			layers = mapView.getLayerManager().getLayers();
			mapView.getModel().mapViewPosition
					.setMapPosition(new MapPosition(tapLatLong, mapView
							.getModel().mapViewPosition.getZoomLevel()));

			// INVIO POSIZIONE AL SERVER QUI, qui come posizione precedente do
			// -1 perch� non ce n'� una precedente
			if (myServer.checkConnection(this)) {
				myServer.changeLOS(myUtente.getUsername(),
						myUtente.getPassword(), messageHandler,
						applicazione.getMySSLContext(), myUtente.getCitta(), 3,
						-1, edgeAttuale.getEdge());
			}

		} else { // Se la risposta non � vuota significa che non � la prima
					// volta, allora calcolo la distanza dal nodo
			valRestituito = true;
			numeroIstruzione = 0;
			Location nodo1 = new Location("");
			nodo1.setLatitude(resultMessage.getPoints().getLatitude(1));
			nodo1.setLongitude(resultMessage.getPoints().getLongitude(1));

			distanza = posizioneAttuale.distanceTo(nodo1);

			// Il confronto tra posizione attuale e precedente  serve nel caso
			// l'utente ha cambiato edge senza avvicinarsi al nodo e per
			// aggiornare la polilyne
			locationIndex = hopper.getLocationIndex();
			queryPosizioneAttuale = locationIndex.findClosest(
					posizioneAttuale.getLatitude(),
					posizioneAttuale.getLongitude(), EdgeFilter.ALL_EDGES);
			queryPosizionePrecedente = locationIndex.findClosest(
					posizionePrecedente.getLatitude(),
					posizionePrecedente.getLongitude(), EdgeFilter.ALL_EDGES);
			edgeAttuale = queryPosizioneAttuale.getClosestEdge();
			edgePrecedente = queryPosizionePrecedente.getClosestEdge();

			if (distanza < RAGGIO)// Questo  serve per settare l'istruzione da
									// visualizzare e per azzerare il nuovoNodo
			{
				numeroIstruzione = 1;
			} else {
				nuovoNodo = true;
			}

			Location destinazioneCorrente = new Location("");
			destinazioneCorrente.setLatitude(end.latitude);
			destinazioneCorrente.setLongitude(end.longitude);
			Float distanzaDestinazione = posizioneAttuale
					.distanceTo(destinazioneCorrente);

			if (distanzaDestinazione <= 3)// In questo caso l'utnte � arrivato al punto di raccolta
			{
				gps.stopUsingGPS();
				popUp("SEI ARRIVATO!", "Complimenti! Sei salvo!");

			} else if (edgeAttuale.getEdge() != edgePrecedente.getEdge()) {
				// Se l'edge precedente � diverso da quello attuale ricalcolo il percorso
				nuovoNodo = true;
				shortestPathRunning = true;
				myMappa.routing(posizioneAttuale.getLatitude(),
						posizioneAttuale.getLongitude(), destinazioni, hopper,
						messageHandler, end, context);
				
				//CENTRO LA MAPPA
				layers = mapView.getLayerManager().getLayers();
				mapView.getModel().mapViewPosition
						.setMapPosition(new MapPosition(tapLatLong, mapView
								.getModel().mapViewPosition.getZoomLevel()));

				//Ho cambiato edge, quindi invio la posizione aggiornata
				if (myServer.checkConnection(this)) {
					myServer.changeLOS(myUtente.getUsername(),
							myUtente.getPassword(), messageHandler,
							applicazione.getMySSLContext(),
							myUtente.getCitta(), 3, edgePrecedente.getEdge(),
							edgeAttuale.getEdge());
				}

			} else if (distanza < RAGGIO && nuovoNodo) {
				// Se sono vicino al nodo ed � un nodo nuovo ricalcolo il percorso

				nuovoNodo = false;// Una volta fatto il primo calcolo non � pi�
				// un nodo nuovo

				// In questo caso devo prima aggiornare i pesi in locale e poi
				// posso fare il ricalcolo
				scaricaLos();

			} else {
				// Se la distanza dal nodo � superiore al raggio impostato o non
				// � un nuovo nodo aggiorno la polilyne
				Layers layers = mapView.getLayerManager().getLayers();
				mapView.getModel().mapViewPosition
						.setMapPosition(new MapPosition(tapLatLong, mapView
								.getModel().mapViewPosition.getZoomLevel()));
				while (layers.size() > 1) {
					layers.remove(1);
				}
				marker = createMarker(tapLatLong, R.drawable.marker_posizione);
				if (marker != null) {
					layers.add(marker);
				}

				marker = createMarker(end, R.drawable.marker_punto_raccolta);
				if (marker != null) {
					mapView.getLayerManager().getLayers().add(marker);
				}

				updateResultMessage();

				mapView.getLayerManager().getLayers().add(createPolyline(resultMessage));
			}
		}

		return valRestituito;
	}

	/*Metodo per l'aggiornamento della polilyne. L'aggiornamento viene fatto aggiornando il primo nodo con la 
	 * posizione dell'utente. Un caso particolare si ha per le strade lunghe che non hanno incroci intermedi
	 * per le quali graphHopper crea dei nodi virtuali per cui deve controllare che l'utente abbia superato o no tali nodi
	*/
	private void updateResultMessage() {
		float dist02 = 0;// distanza tra n0(la mia posizione) e n2
		float dist12 = 0;// distanza tra n1 e n2
		float distedgeto1 = 0;// distanza tra il punto a me pi� vicino su un
								// edge e n1

		n0.setLatitude(posizioneAttuale.getLatitude());
		n0.setLongitude(posizioneAttuale.getLongitude());

		virtualPosition
				.setLatitude(queryPosizioneAttuale.getSnappedPoint().lat);
		virtualPosition
				.setLongitude(queryPosizioneAttuale.getSnappedPoint().lon);
		
		//Nel caso la polilyne � stata modificata precedentamente, rieffettuo le modifiche
		for (int i = 0; i <= nodiPolylineModificati; i++) {
			resultMessage.getPoints().setNode(i,
					queryPosizioneAttuale.getSnappedPoint().lat,
					queryPosizioneAttuale.getSnappedPoint().lon);
		}

		do {
			if (nodiPolylineModificati < resultMessage.getPoints().size() - 2) {
				n1.setLatitude(resultMessage.getPoints().getLatitude(
						nodiPolylineModificati + 1));
				n1.setLongitude(resultMessage.getPoints().getLongitude(
						nodiPolylineModificati + 1));

				n2.setLatitude(resultMessage.getPoints().getLatitude(
						nodiPolylineModificati + 2));
				n2.setLongitude(resultMessage.getPoints().getLongitude(
						nodiPolylineModificati + 2));

				dist02 = n0.distanceTo(n2);
				dist12 = n1.distanceTo(n2);
				distedgeto1 = virtualPosition.distanceTo(n1);
				
				//Se ho superato il nodo successivo allora cambio il suo valore con la posizione dell'utente
				if (dist02 < dist12
						&& dist12 - (dist02 + distedgeto1) < TOLLERANZA) {
					resultMessage.getPoints().setNode(
							nodiPolylineModificati + 1,
							queryPosizioneAttuale.getSnappedPoint().lat,
							queryPosizioneAttuale.getSnappedPoint().lon);

					nodiPolylineModificati++;
				}
			}
		} while (dist02 < dist12);
	}
	
	//Metodo per il download dei Los aggironati dal server
	private void scaricaLos() {
		if (myServer.checkConnection(this)) {
			serverDown = 0;
			// Se ho connessione faccio la chiamata al server per aggiornare i
			// LOS.
			myServer.getLOS(myUtente.getUsername(), myUtente.getPassword(),
					messageHandler, applicazione.getMySSLContext(),
					myUtente.getCitta(), 2);
		} else {
			// Se non ho connessione la prima volta riazzero i los e setto i
			// pesi degli archi ai valori standard
			serverDown++;
			if (serverDown == 1) {
				// Riazzero i los
				for (Integer key : mappaLOS.keySet()) {
					mappaLOS.put(key, 0.00);
				}
				applicazione.setMappaLOS(mappaLOS);
			}
			// Rifaccio il routing poich� anche nel caso non � la prima volta
			// con il server down questa funzione la chiamo solo la prima volta
			// che entro nel raggio
			shortestPathRunning = true;
			myMappa.routing(posizioneAttuale.getLatitude(),
					posizioneAttuale.getLongitude(), destinazioni, hopper,
					messageHandler, end, context);

		}
	}

	private void finishPrepare() {
		prepareInProgress = false;
	}
	
	/*Metodo per la creazione della polilyne (Visualizzazione del percorso) a partire dal GHResponse
	 * ovvero dalla risposta di graphhopper
	*/
	private Polyline createPolyline(GHResponse response) {
		Paint paintStroke = AndroidGraphicFactory.INSTANCE.createPaint();
		paintStroke.setStyle(Style.STROKE);
		paintStroke.setColor(Color.argb(200, 0, 0xCC, 0x33));
		paintStroke.setDashPathEffect(new float[] { 25, 15 });
		paintStroke.setStrokeWidth(12);

		Polyline line = new Polyline(
				(org.mapsforge.core.graphics.Paint) paintStroke,
				AndroidGraphicFactory.INSTANCE);
		List<LatLong> geoPoints = line.getLatLongs();
		PointList tmp = response.getPoints();
		for (int i = 0; i < response.getPoints().getSize(); i++) {
			geoPoints.add(new LatLong(tmp.getLatitude(i), tmp.getLongitude(i)));
		}

		return line;
	}
	
	//Metodo per la creazione di un marker sulla mappa
	private Marker createMarker(LatLong p, int resource) {
		Drawable drawable = getResources().getDrawable(resource);
		Bitmap bitmap = AndroidGraphicFactory.convertToBitmap(drawable);
		return new Marker(p, bitmap, 0, -bitmap.getHeight() / 2);
	}

	private boolean isReady() {
		Boolean ris;
		// Restutuisce true solo se il caricamento � terminato
		if (hopper != null)
			ris = true;

		else if (prepareInProgress) {
			// logUser("Preparation still in progress");
			ris = false;
		} else {
			ris = false;
			logUser("Errore nel caricamento dei file");
		}
		return ris;
	}

	//MEtodo per la visualizzazione dei PopUp
	private void popUp(String titolo, String corpo) {
		alertDialogBuilder.setTitle(titolo).setMessage(corpo)
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
		alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		gps.stopUsingGPS();
		finish();
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onRestart();
		gps = new Geolocalizzazione(MappaActivity.this, messageHandler, false);
	}
}