// Activity che contiene la schermata principale dell'applicazione da cui l'utente
// pu� accedere a tutte le funzionalit�

package com.gruppo1.iotforemergency;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import com.gruppo1.gestioneutente.IntUtente;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MenuActivity extends AppCompatActivity {
	
	private MyApplication applicazione;
	private IntUtente myUtente;
	private Boolean ultimoAggiornamento;
	private AlertDialog.Builder alertDialogBuilder;
	private AlertDialog alertDialog;
	private final String erroreObsoleto = "� passato molto tempo dall'ultima volta che i dati sono stati aggiornati. Eseguire il download per assicurarsi di avere l'ultima versione";
	private static final int SETTINGS_REQUEST = 1;
	private static final int DOWNLOAD_REQUEST = 2;
	private static final int UPDATE_REQUEST = 3; //Un codice diverso per l'aggiornamento (anche se � la stessa activity) cos� l'app non viene chiusa in caso di errore. 
	
	// Inizializza lo stato dell'activity
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		applicazione = (MyApplication) this.getApplication();
		myUtente = applicazione.getMyUtente();
		alertDialogBuilder = new AlertDialog.Builder(this);
		
		TextView nick = (TextView) findViewById(R.id.textNick);
		nick.setText(Html.fromHtml("Ciao <b>" + myUtente.getUsername() + "</b>"));
		
		Button cambiaLocalita = (Button) findViewById(R.id.cambia_localita);
		
		Log.d("IOT", "Last Update dell'utente: " + myUtente.getLastUpdate());
		obsoleto(myUtente.getLastUpdate());
		Button buttonCalcola = (Button) findViewById(R.id.buttonCalcPercorso);
		Button buttonAggiorna = (Button) findViewById(R.id.buttonAggiornamento);
		
		buttonCalcola.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent visualizzaMappa = new Intent(MenuActivity.this, MappaActivity.class);
				startActivity(visualizzaMappa);
			}
		});
		
		buttonAggiorna.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent avviaAggiornamento = new Intent(MenuActivity.this, DownloadActivity.class);
				startActivityForResult(avviaAggiornamento, UPDATE_REQUEST);
			}
		});
		
		cambiaLocalita.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent settings = new Intent(MenuActivity.this, SettingActivity.class);
				startActivityForResult(settings, SETTINGS_REQUEST);
			}
		});
	}
	
	// Metodo per visualizzare la citt� corrente sull'interfaccia
	@Override
	protected void onResume() {
		super.onResume();
		TextView cittaAttuale = (TextView) findViewById(R.id.citta_attuale);
		cittaAttuale.setText("Citt� attuale: " + myUtente.getCitta());
	}

	// Viene utilizzato quando un'altra activity viene chiusa e lascia un risultato da elaborare
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK) {
			if(requestCode == SETTINGS_REQUEST) {
				Intent avviaAggiornamento = new Intent(MenuActivity.this, DownloadActivity.class);
				startActivityForResult(avviaAggiornamento, DOWNLOAD_REQUEST);
			}			
		}else if(resultCode == RESULT_CANCELED) {
			if(requestCode == DOWNLOAD_REQUEST) {
				finish();
			}
		}
	}
	
	// Metodo che visualizza un pop up con un certo messaggio e in seguito chiude l'activity
	private void popUp(String titolo, String corpo){
		alertDialogBuilder
		.setTitle(titolo)
		.setMessage(corpo)
		.setCancelable(false)
		.setPositiveButton("Ok",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
         alertDialog = alertDialogBuilder.create();

         alertDialog.show();
	}	
	
	// Metodo che verifica se l'ultimo aggiornamento dell'utente � stato effettuato parecchio tempo fa (150 giorni)
	private void obsoleto (int versione) {
		String versioneString = Integer.toString(versione);
		int year = Integer.parseInt(versioneString.substring(0, 4));
		int month = Integer.parseInt(versioneString.substring(4, 6)) - 1; //perch� i mesi sono zero-based
		int day = Integer.parseInt(versioneString.substring(6, 8));
		Date last = new GregorianCalendar(year, month, day).getTime();
		Date today = new Date();
		long daysDiff = (today.getTime() - last.getTime()) / (1000 * 60 * 60 * 24);
		//Log.d("IOT", "Oggi: " + today.toString() + " Last Update: " + last.toString() + " Differenza: " + daysDiff);
		//Differenza di almeno 150 giorni
		if(daysDiff > 150) {
			popUp("Attenzione", erroreObsoleto);
		}
	}
}
