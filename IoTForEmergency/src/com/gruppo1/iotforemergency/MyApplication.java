//Classe utilizzata per condividere alcuni oggetti tra le varie activity

package com.gruppo1.iotforemergency;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import com.graphhopper.util.EdgeIteratorState;
import com.gruppo1.gestionemappa.IntMappa;
import com.gruppo1.gestioneserver.IntServer;
import com.gruppo1.gestioneutente.IntUtente;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {
	
	private IntUtente MyUtente;
	private IntServer MyServer;
	private IntMappa MyMappa;
	private SSLContext MySSLContext;
    private Map<Integer, Double> mappaPesiFissi ;
    private Map<Integer, Double> mappaLOS;
    private EdgeIteratorState edgeAttuale;
    private int contatoreEdge;
	
    @Override
	public void onCreate(){
        super.onCreate();
        mappaLOS = new HashMap<Integer, Double>();
        mappaPesiFissi = new HashMap<Integer, Double>();
    }
    
	public IntUtente getMyUtente() {
		return MyUtente;
	}

	public void setMyUtente(IntUtente myUtente) {
		MyUtente = myUtente;
	}

	public IntServer getMyServer() {
		return MyServer;
	}

	public void setMyServer(IntServer myServer) {
		MyServer = myServer;
	}

	public IntMappa getMyMappa() {
		return MyMappa;
	}

	public void setMyMappa(IntMappa myMappa) {
		MyMappa = myMappa;
	}
	
	public SSLContext getMySSLContext() {
		return MySSLContext;
	}

	public void setMySSLContext(SSLContext mySSLContext) {
		MySSLContext = mySSLContext;
	}
	
	public Map<Integer, Double> getMappaPesiFissi() {
		return mappaPesiFissi;
	}

	public void setMappaPesiFissi(Map<Integer, Double> mappaPesiFissi) {
		this.mappaPesiFissi = mappaPesiFissi;
	}

	public Map<Integer, Double> getMappaLOS() {
		return mappaLOS;
	}

	public void setMappaLOS(Map<Integer, Double> mappaLOS) {
		this.mappaLOS = mappaLOS;
	}
	
	public Double getPesoFisso(int id)
	{
		Double peso=-1.00;
		if(mappaPesiFissi.containsKey(id))
		{
			peso=mappaPesiFissi.get(id);
		}
		return peso;
	}
	
	public Double getLos(int id)
	{
		Double los=-1.00;
		if(mappaLOS.containsKey(id))
		{
			los=mappaLOS.get(id);
		}
		return los;
	}

	public EdgeIteratorState getEdgeAttuale() {
		return edgeAttuale;
	}

	public void setEdgeAttuale(EdgeIteratorState edgeAttuale) {
		this.edgeAttuale = edgeAttuale;
	}

	public int getContatoreEdge() {
		return contatoreEdge;
	}

	public void setContatoreEdge(int contatoreEdge) {
		this.contatoreEdge = contatoreEdge;
	}
	
}
