// Activity che mostra la form di registrazione e avvia il processo di registrazione

package com.gruppo1.iotforemergency;

import com.gruppo1.gestioneserver.IntServer;
import com.gruppo1.gestioneutente.IntUtente;
import com.gruppo1.gestioneutente.Utente;
import com.gruppo1.model.DbAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;

public class RegistrazioneActivity extends AppCompatActivity {

	private Boolean campiVuoti;
	private Boolean passwordOK;
	private final Context context = this;
	private IntServer myServer;
	private IntUtente myUtente;
	private MyApplication applicazione;
	private AlertDialog.Builder alertDialogBuilder;
	private AlertDialog alertDialog;
	private String resultMessage;
	private int dialogEnd;
	private DbAdapter dbAdapter; 
    private Cursor cursor;
    private EditText[] editRegistrazione;
	private String[] stringRegistrazione;
	private String passwordError;
    
	private Handler messageHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	resultMessage = msg.obj.toString();
        	switch (msg.what) {
			case -1:
				dialogEnd = 0;
				if(resultMessage.split("-")[0].compareTo("409") == 0) {
					editRegistrazione[2].setError(resultMessage.split("-")[1]);
					editRegistrazione[2].requestFocus();
				}
				popUp("Errore!",resultMessage.split("-")[1]);
				break;
			case 0:
				dialogEnd = 1;	
        		//Salvataggio dei dati sul DB locale
        		dbAdapter = new DbAdapter(context);
        	    dbAdapter.open();	    
        	    dbAdapter.insertUtente(stringRegistrazione[2], stringRegistrazione[0], stringRegistrazione[1], stringRegistrazione[3],null, 0, 0);
        	    dbAdapter.close();
    
        	    //Creo la classe Utente
                myUtente = new Utente(stringRegistrazione[2],stringRegistrazione[3],null, 0, 0);
                applicazione.setMyUtente(myUtente); 		
				popUp("Benvenuto!",resultMessage);
				break;
			}
        }
    };
    
    // Inizializza l'activity e la form
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registrazione);
		
		applicazione= (MyApplication) this.getApplication();
		myServer = applicazione.getMyServer();
		alertDialogBuilder = new AlertDialog.Builder(context);
		Button buttonRegistrati = (Button) findViewById(R.id.buttonRegistrati);

		buttonRegistrati.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				campiVuoti = false;
				passwordOK = true;
				passwordError = "";
				
				/*
				 Campi Form:
				 0-Nome
				 1-Cognome
				 2-Username
				 3-Password
				 4-Conferma Password
				*/
				
				editRegistrazione = new EditText[5];
				stringRegistrazione = new String[5];
				
				editRegistrazione[0] = (EditText) findViewById(R.id.editNome);
				editRegistrazione[1] = (EditText) findViewById(R.id.editCognome);
				editRegistrazione[2] = (EditText) findViewById(R.id.editUsername);
				editRegistrazione[3] = (EditText) findViewById(R.id.editPassword);
				editRegistrazione[4] = (EditText) findViewById(R.id.editConfermaPassword);
				
				//Scorro al contrario cos� l'ultima edit a cui viene trovato l'errore (e quindi ha il focus) � quella pi� in alto
				for (int i = editRegistrazione.length - 1; i >= 0; i--) {
					stringRegistrazione[i] = editRegistrazione[i].getText().toString();
					editRegistrazione[i].setError(null);
					if(stringRegistrazione[i].isEmpty()) {
						campiVuoti = true;
						editRegistrazione[i].setError("Questo campo non pu� essere vuoto.");
						editRegistrazione[i].requestFocus();
					}
				}
				
				if((campiVuoti == false) && (stringRegistrazione[3].length() < 6)) {
					passwordError = "La password deve contenere almeno 6 caratteri.";
					passwordOK=false;
					editRegistrazione[3].setError(passwordError);
					editRegistrazione[3].requestFocus();
				} else if((campiVuoti == false) && !(stringRegistrazione[3].equals(stringRegistrazione[4]))) {
					passwordError = "Le due password non coincidono.";
					passwordOK=false;
					editRegistrazione[4].setError(passwordError);
					editRegistrazione[4].requestFocus();
				}				
				
				if (!campiVuoti && passwordOK) {					
					myServer.registrazione(stringRegistrazione[2], stringRegistrazione[0], stringRegistrazione[1], stringRegistrazione[3], applicazione.getMySSLContext(), messageHandler);
				} 
				else if(campiVuoti) {
					dialogEnd = 0;
					popUp("Attenzione!","Tutti i campi sono obbligatori.");
				} 
				else if(!passwordOK) {
					dialogEnd = 0;
					popUp("Attenzione!", passwordError);
				}
			}
		});
		
		//Nasconde la tastiera con un tap fuori da una EditText
		TableLayout layout = (TableLayout) findViewById(R.id.TableLayout1);
		layout.setOnClickListener(new OnClickListener() {
		    @Override
		    public void onClick(View view) {
	    		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			    in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		    }
		});
	}
	
	// Mostra un pop up con un particolare messaggio e chiude il pop up o l'activity
	private void popUp(String titolo, String corpo){
		alertDialogBuilder
		.setTitle(titolo)
		.setMessage(corpo)
		.setCancelable(false)
		.setPositiveButton("Ok",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					if (dialogEnd == 0) {
						dialog.cancel();
					}
					else if (dialogEnd == 1) {
						Intent resultIntent = new Intent();
						setResult(RESULT_OK, resultIntent);
						finish();
					}
				}
			});
         alertDialog = alertDialogBuilder.create();

         alertDialog.show();
		
	}	
	
}
