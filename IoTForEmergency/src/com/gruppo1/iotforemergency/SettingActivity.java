// Activity che permette di modificare la modalit� di localizzazione dell'utente

package com.gruppo1.iotforemergency;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gruppo1.gestionemappa.IntMappa;
import com.gruppo1.gestioneserver.IntServer;
import com.gruppo1.gestioneutente.IntUtente;
import com.gruppo1.model.DbAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;

public class SettingActivity extends AppCompatActivity {
	
	private IntServer myServer;
	private IntUtente myUtente;
	private IntMappa myMappa;
	private MyApplication applicazione;
	private Context context;
	private Spinner sceltaCitta;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> cittaDisponibili;
	private AlertDialog.Builder alertDialogBuilder;
	private AlertDialog alertDialog;
	private RadioGroup modeLocalizzazione;
	private RadioButton automaticMode;
	private RadioButton manualMode;
	private Button saveButton;
	private Boolean automatic;
	private int spinnerPostion;
	private String citta;
	private DbAdapter dbAdapter;
	private Boolean forceManual;
    
    private Handler messageHandler = new Handler() {
    	/*
    	 * Case -1 : Errore
    	 * Case 0: Risposta con la lista delle citt� disponibili
    	 * */
        @Override
        public void handleMessage(Message msg) {
        	switch(msg.what) {
        	case -1 :         
        		popUp("Errore!", msg.obj.toString().split("-")[1]);
        		break;
        	case 0 :
        		cittaDisponibili.remove(0);
        		JSONObject jsonObj;
				try {
					jsonObj = new JSONObject((String) msg.obj);
					JSONArray cityList = jsonObj.getJSONArray("citta");
					cittaDisponibili.clear();
					for(int i = 0; i < cityList.length(); i++) {
						cittaDisponibili.add(cityList.getString(i));
					}
					adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, cittaDisponibili);
					sceltaCitta.setAdapter(adapter);
					sceltaCitta.setEnabled(true);
					if(!citta.isEmpty()) {
						spinnerPostion = adapter.getPosition(citta);
						sceltaCitta.setSelection(spinnerPostion);
					}
				} catch (JSONException e) {
					Log.d("IOT", "Errore durante il parsing delle citt� disponibili: " + e.toString());
					popUp("Errore!", "Errore durante la lettura delle citt� disponibili. Riprova pi� tardi");
				}
        		break;
    		}
        }
    };
	
    // Inizializza l'activity
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		applicazione = (MyApplication) this.getApplication();
		myServer = applicazione.getMyServer();
		myUtente = applicazione.getMyUtente();
		myMappa = applicazione.getMyMappa();
		
		context = this;
		
		citta = "";
		
		alertDialogBuilder = new AlertDialog.Builder(this);
		
		sceltaCitta = (Spinner) findViewById(R.id.cittaDisponibili);
		cittaDisponibili = new ArrayList<String>();
		cittaDisponibili.add("Citt� disponibili");
		adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, cittaDisponibili);
		sceltaCitta.setAdapter(adapter);
		sceltaCitta.setEnabled(false);
		
		sceltaCitta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(parent.getItemAtPosition(position).toString().compareTo("Citt� disponibili") != 0) {
					citta = parent.getItemAtPosition(position).toString();
				}
				Log.d("IOT", parent.getItemAtPosition(position).toString());
			}
			 
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		
		modeLocalizzazione = (RadioGroup) findViewById(R.id.sceltaModalita);
		automaticMode  = (RadioButton)findViewById(R.id.automaticMode);
        manualMode  = (RadioButton)findViewById(R.id.manualMode);
        
        Log.d("IOT", "Modalit� localizzazione: " + myUtente.getModeLocalizzazione());
        if(myUtente.getModeLocalizzazione() == 1) {
        	modeLocalizzazione.check(manualMode.getId());
        }
        
        selectedRadio();
        modeLocalizzazione.setOnCheckedChangeListener(new OnCheckedChangeListener() {
        	@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
        		selectedRadio();
        	}
        });
        
        saveButton = (Button) findViewById(R.id.salvaImpostazioni);
        
        saveButton.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				dbAdapter = new DbAdapter(context);
				if((myUtente.getModeLocalizzazione() == 0) && (automatic == true)) {
					closeActivity(RESULT_CANCELED);
				}
				if((myUtente.getModeLocalizzazione() == 0) && (automatic == false)){
					resetDownlaod(citta, 1);
				}
				if((myUtente.getModeLocalizzazione() == 1) && (automatic == true)){
					resetDownlaod("", 0);
				}
				if((myUtente.getModeLocalizzazione() == 1) && (automatic == false)){
					if(myUtente.getCitta().compareTo(citta) == 0) {
						closeActivity(RESULT_CANCELED);
					} else {
						resetDownlaod(citta, 1);
					}
				}
			}
		});
	}
	
	// Se la modalit� di localizzazione viene cambiata viene chiusa l'activity in modo che venga eseguito il download
	private void resetDownlaod(String citta, int mode) {
		Log.d("IOT", "La modalit� scelta �: " + Integer.toString(mode) + " e la citt� selezionata �: " + citta);
		myUtente.setCitta(citta);
		myUtente.setModeLocalizzazione(mode);
		myMappa.setVersioneDati(0);
		myMappa.setVersioneMappa(0);
		try {
			dbAdapter.open();	    
			dbAdapter.updateCitta(citta, myUtente.getUsername());
			dbAdapter.updateModeLocalizzazione(mode, myUtente.getUsername());
			dbAdapter.updateVersioneDati(0);
			dbAdapter.updateVersioneMappa(0);
			dbAdapter.close();
			closeActivity(RESULT_OK);
		}
		catch (Exception e) {
			Log.d("IOT", "Problema durante il salvataggio dei nuovi valori: " + e.toString());
			popUp("Errore!", "Problema durante il salvataggio dei nuovi valori. Riprova pi� tardi");
		}
	}
	
	// Metodo che verifica quale soluzione � stata scelta
	private void selectedRadio() {
		if(manualMode.isChecked()) {
			automatic = false;
			citta = myUtente.getCitta();
			myServer.getCittaDisponibili(myUtente.getUsername(), myUtente.getPassword(), messageHandler, applicazione.getMySSLContext(), 0);
		}
		if(automaticMode.isChecked()) {
			sceltaCitta.setEnabled(false);
			automatic = true;
		}
	}
	
	// Visualizza un pop up con un certo messaggio e chiude l'activity
	private void popUp(String titolo, String corpo){
		alertDialogBuilder
		.setTitle(titolo)
		.setMessage(corpo)
		.setCancelable(false)
		.setPositiveButton("Ok",
			new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					closeActivity(RESULT_CANCELED);
				}
			});
         alertDialog = alertDialogBuilder.create();

         alertDialog.show();
	}
	
	// Metodo che chiude l'activity
	private void closeActivity(int result) {
		Intent resultIntent = new Intent();
		setResult(result, resultIntent);
		finish();
	}
}
