//Classe contenente tutte le possibili query al DB che vengono eseguite sfruttando la classe DbHelper
package com.gruppo1.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DbAdapter {
	
	  private Context context;
	  private SQLiteDatabase database;
	  private DbHelper dbHelper;
	 
	  // Database fields
	  private static final String TABLE_UTENTE = "utente";
	  private static final String TABLE_VERSIONE = "versione";
	  private static final String TABLE_PESO = "peso";
	  private static final String TABLE_RACCOLTA = "raccolta";
	  
	  public static final String UTENTE_KEY_NICK = "nick";
	  public static final String UTENTE_KEY_NOME = "nome";
	  public static final String UTENTE_KEY_COGNOME = "cognome";
	  public static final String UTENTE_KEY_PASSWORD = "password";
	  public static final String UTENTE_KEY_CITTA = "citta";
	  public static final String UTENTE_KEY_LASTUPDATE = "lastUpdate";
	  public static final String UTENTE_KEY_MODELOCALIZZAZIONE = "modeLocalizzazione";
	  
	  public static final String VERSIONE_KEY_ID = "id";
	  public static final String VERSIONE_KEY_MAPPA = "versioneMappa";
	  public static final String VERSIONE_KEY_DATI = "versioneDati";
	  
	  public static final String PESO_KEY_EDGEID = "edgeId";
	  public static final String PESO_KEY_CITTA = "citta";
	  public static final String PESO_KEY_K = "k";
	  
	  public static final String RACCOLTA_KEY_ID = "id";
	  public static final String RACCOLTA_KEY_CITTA = "citta";
	  public static final String RACCOLTA_KEY_LAT = "lat";
	  public static final String RACCOLTA_KEY_LON = "lon";
	  
	//Costruttore che prende in input il context dell'applicazione  
	public DbAdapter(Context context) {
		this.context = context;
	}
	
	//Metodo per la lettura del file del DB
	public DbAdapter open() throws SQLException {
		dbHelper = new DbHelper(context);
		database = dbHelper.getWritableDatabase();
		return this;
	}
	
	//Metodo per la chiusura del collegamento al DB
	public void close() {
		dbHelper.close();
	}
	
	//UTENTE
	
	//Inserisce un utente
	public long insertUtente(String nick, String nome, String cognome, String password, String citta, int lastUpdate, int modeLocalizzazione) {
		ContentValues values = new ContentValues();
	    values.put(UTENTE_KEY_NICK, nick );
	    values.put(UTENTE_KEY_NOME, nome );
	    values.put(UTENTE_KEY_COGNOME, cognome);
	    values.put(UTENTE_KEY_PASSWORD, password );
	    values.put(UTENTE_KEY_LASTUPDATE, lastUpdate);
	    values.put(UTENTE_KEY_CITTA, citta );
	    values.put(UTENTE_KEY_MODELOCALIZZAZIONE, modeLocalizzazione);
	    return database.insertOrThrow(TABLE_UTENTE, null, values);
	}
	 
	//Estrae tutti gli utenti
	public Cursor selectUtenti() {
	    return database.query(TABLE_UTENTE, new String[] { UTENTE_KEY_NICK, UTENTE_KEY_NOME, UTENTE_KEY_COGNOME, UTENTE_KEY_PASSWORD, UTENTE_KEY_CITTA, UTENTE_KEY_LASTUPDATE, UTENTE_KEY_MODELOCALIZZAZIONE}, null, null, null, null,null);
	}
	   
	//Estrae un utente in particolare
	public Cursor selectUtente(String nick) {
	    Cursor mCursor = database.query(true, TABLE_UTENTE, new String[] {
	                                    UTENTE_KEY_NICK, UTENTE_KEY_NOME, UTENTE_KEY_COGNOME, UTENTE_KEY_PASSWORD, UTENTE_KEY_CITTA, UTENTE_KEY_LASTUPDATE, UTENTE_KEY_MODELOCALIZZAZIONE},
	                                    UTENTE_KEY_NICK + " = '"+ nick+"'", null, null, null, null,null);
	    return mCursor;
	}
	  
	//Aggiorna il lastUpdate
	public boolean updateLastUpdate(String nick, int lastUpdate) {
		ContentValues updateValues = new ContentValues();
		updateValues.put(UTENTE_KEY_LASTUPDATE, lastUpdate);
		return database.update(TABLE_UTENTE, updateValues, UTENTE_KEY_NICK + "='" + nick+"'", null) > 0;
	}
		
	//Aggiorna la citt� dell'utente
	public boolean updateCitta(String citta, String nickutente) {
	    ContentValues updateValues = new ContentValues();
	    updateValues.put(UTENTE_KEY_CITTA, citta);
	    return database.update(TABLE_UTENTE, updateValues,  UTENTE_KEY_NICK + " = '"+ nickutente+"'", null) > 0;
	}
	
	//Aggiorna il valore del campo modeLocalizzazione
	public boolean updateModeLocalizzazione(int valore, String nickutente) {
		ContentValues updateValues = new ContentValues();
	    updateValues.put(UTENTE_KEY_MODELOCALIZZAZIONE, valore);
	    return database.update(TABLE_UTENTE, updateValues,  UTENTE_KEY_NICK + " = '"+ nickutente+"'", null) > 0;
	}

	//VERSIONE
	  
	//Estrae la versione della mappa e dei dati
	public Cursor selectVersioni () {
		  Cursor mCursor = database.query(true, TABLE_VERSIONE, new String[] {
			  VERSIONE_KEY_MAPPA, VERSIONE_KEY_DATI},
			  VERSIONE_KEY_ID + " = '" + 0 + "'", null, null, null, null,null);
		  return mCursor;
	}
	  
	//Aggiorna il timestamp della mappa
	public boolean updateVersioneMappa(int versione) {
	    ContentValues updateValues = new ContentValues();
	    updateValues.put(VERSIONE_KEY_MAPPA, versione);
	    return database.update(TABLE_VERSIONE, updateValues, VERSIONE_KEY_ID + "='" + 0+"'", null) > 0;
	}
	  
	//Aggiorna il timestamp dei dati
	public boolean updateVersioneDati(int versione) {
	    ContentValues updateValues = new ContentValues();
	    updateValues.put(VERSIONE_KEY_DATI, versione);
	    return database.update(TABLE_VERSIONE, updateValues, VERSIONE_KEY_ID + "='" + 0+"'", null) > 0;
	}
	  
	//PESO
	  
	//Inserisci un peso 
	public long insertPeso (int idEdge, String citta, double k) {
		  ContentValues values = new ContentValues();
		    values.put(PESO_KEY_EDGEID, idEdge );
		    values.put(PESO_KEY_CITTA, citta );
		    values.put(PESO_KEY_K, k);
		    return database.insertOrThrow(TABLE_PESO, null, values);
	}
	  
	//Cancella pesi data una citt�      
	public boolean deletePesi() {
	    return database.delete(TABLE_PESO, "1", null) > 0;
	}
	
	// Estrae tutti i pesi di una citt�
    public Cursor selectPesiByCitta(String citta) {
        Cursor mCursor = database.query(true, TABLE_PESO, new String[] {
                PESO_KEY_EDGEID, PESO_KEY_K }, PESO_KEY_CITTA + " = '" + citta
                + "'", null, null, null, null, null);
        return mCursor;
    }
	  
	//PUNTI DI RACCOLTA
	  
	//Inserisci un punto di raccolta 
	public long insertRaccolta (int id, String citta, double lat, double lon) {
		  ContentValues values = new ContentValues();
		    values.put(RACCOLTA_KEY_ID, id );
		    values.put(RACCOLTA_KEY_CITTA, citta );
		    values.put(RACCOLTA_KEY_LAT, lat);
		    values.put(RACCOLTA_KEY_LON, lon);
		    return database.insertOrThrow(TABLE_RACCOLTA, null, values);
	}
	  
	//Cancella punti di raccolta data una citt�      
	public boolean deleteRaccolta() {
		  return database.delete(TABLE_RACCOLTA, "1", null) > 0;
	}

    // Estrae tutti i punti di raccolta di una citt�
    public Cursor selectPuntiRaccoltaByCitta(String citta) {
        Cursor mCursor = database.query(true, TABLE_RACCOLTA, new String[] {
                RACCOLTA_KEY_ID, RACCOLTA_KEY_LAT, RACCOLTA_KEY_LON },
                RACCOLTA_KEY_CITTA + "='" + citta + "'", null, null, null,
                null, null);
        return mCursor;
    }
}
