/*Classe per la creazione del file del DB e delle tabelle al suo interno. Tale classe funge da interfaccia verso il DB
 * e fa uso di SQLite
 * 
 */
package com.gruppo1.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "iot.db";
    private static final int DATABASE_VERSION = 1;
    
    // Lo statement SQL di creazione del database
    private static final String DATABASE_CREATE_UTENTE = "CREATE TABLE utente (nick TEXT PRIMARY KEY, nome TEXT, cognome TEXT, password TEXT, citta TEXT, lastUpdate INT, modeLocalizzazione INT);";
    private static final String DATABASE_CREATE_VERSIONE = "CREATE TABLE versione (id INT PRIMARY KEY, versioneMappa INT, versioneDati INT);";
    private static final String DATABASE_CREATE_PESI = "CREATE TABLE peso (edgeId INT, citta TEXT, k REAL, PRIMARY KEY(edgeId, citta))";
    private static final String DATABASE_CREATE_RACCOLTA = "CREATE TABLE raccolta (id INT, citta TEXT, lat REAL, lon REAL, PRIMARY KEY(id, citta))";
    
	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	//Vengono eseguite le query di creazione del DB
	 @Override
     public void onCreate(SQLiteDatabase database) {
         database.execSQL(DATABASE_CREATE_UTENTE);
         database.execSQL(DATABASE_CREATE_VERSIONE);
         database.execSQL(DATABASE_CREATE_PESI);
         database.execSQL(DATABASE_CREATE_RACCOLTA);
         
         //All'interno della tabella "versione" si settano le versione di mappa e dati a zero
	     ContentValues values = new ContentValues();
	     values.put("id", 0);
	     values.put("versioneMappa" , 0);
	     values.put("versioneDati" , 0);
	     database.insert("versione", null, values);
     }
	 
	 @Override
     public void onUpgrade( SQLiteDatabase database, int oldVersion, int newVersion ) {
         database.execSQL("DROP TABLE IF EXISTS utente");
         database.execSQL("DROP TABLE IF EXISTS versione");
         database.execSQL("DROP TABLE IF EXISTS peso");
         database.execSQL("DROP TABLE IF EXISTS raccolta");
         onCreate(database);
              
     }

}
