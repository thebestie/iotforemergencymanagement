package com.gruppo1;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gruppo1.model.OntologiaCitta;

/*Questa classe si occupa della gestione delle citt� presenti all'interno del server.
 *In particolare permette di inserire nuove citt� mappate, con le relative meta-informazioni.
 *Permette di ottenere informazioni riguado l'obsolescenza dei dati e le ultime versioni aggiornate
 *presenti sul server 
 **/

@Path("citta")
public class Citta {
	
	//Metodo per registrare una nuova citt� all'interno del server
	@RolesAllowed("admin")
	@POST
	public String inserisciCitta(@FormParam("nome") String nome,@FormParam("versioneMappa") int versioneMappa,@FormParam("versionePesi") int versionePesi){
		if(OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.CONFLICT)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� "+nome+" esiste gi�"))
					.build());
		}
		OntologiaCitta.inserisciCitta(nome, versioneMappa, versionePesi);
		return "Citt� inserita correttamente";
	}
	
	//Restituisce l'elenco delle citt� disponibili
	@RolesAllowed({"utente","admin"})
	@GET
	@Produces("aplication/json")
	public String getCitta() throws JSONException{
		JSONObject citta = new JSONObject();
		JSONArray array = new JSONArray(OntologiaCitta.getCitta());
		citta.put("citta",array);
		return citta.toString();
 	}
	
	
	/*Restituisce true se la versione della mappa inoltrata risulta obsoleta
	 *nome=nome della citt� di interesse
	 *verisone=versione locale all'all'applicazione della mappa*/	
	@RolesAllowed({"utente","admin"})
	@GET
	@Path("mappaObsoleta")
	public Boolean mappaObsoleta(@QueryParam("nome") String nome,@QueryParam("versione") int versione){
		if(!OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� richiesta non esiste"))
					.build());
		}
		return (OntologiaCitta.getVersioneMappa(nome)>versione);
	}
	
	//Analogo a mappaObsoleta, ma la versione di interesse � quella dei pesi
	@RolesAllowed({"utente","admin"})
	@GET
	@Path("pesiObsoleti")
	public Boolean pesiObsoleti(@QueryParam("nome") String nome,@QueryParam("versione") int versione){
		if(!OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� richiesta non esiste"))
					.build());
		}
		return(OntologiaCitta.getVersionePesi(nome)>versione);
	}
	
	//Permette all'amministratore di aggiornare la versione della mappa
	@RolesAllowed("admin")
	@PUT
	@Path("versioneMappa")
	public String aggiornaVersioneMappa(@FormParam("nome") String nome,@FormParam("versione") int versione){
		if(!OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� richiesta non esiste"))
					.build());
		}else if(!OntologiaCitta.aggiornaVersioneMappa(nome, versione)){
			throw new WebApplicationException(Response 
					.status(Status.BAD_REQUEST)
					.header("IOT", StringEscapeUtils.escapeHtml4("Versione non aggiornata. La versione attuale della mappa � pi� recente."))
					.build());
		}
		return "La versione della mappa � stata aggiornata correttamente";
	}
	
	//Permette all'amministratore di aggiornare la versione dei pesi
	@RolesAllowed("admin")
	@PUT
	@Path("versionePesi")
	public String aggiornaVersionePesi(@FormParam("nome") String nome,@FormParam("versione") int versione){
		if(!OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� richiesta non esiste"))
					.build());
		}else if(!OntologiaCitta.aggiornaVersionePesi(nome, versione)){
			throw new WebApplicationException(Response 
					.status(Status.BAD_REQUEST)
					.header("IOT", StringEscapeUtils.escapeHtml4("Versione non aggiornata. La versione attuale dei pesi � pi� recente."))
					.build());
		}
		return "La versione dei pesi � stata aggiornata correttamente";
	}
	
	//Metodo che restituisce la versione attuale della mappa, relativa alla citt� in argomento
	@RolesAllowed({"utente","admin"})
	@GET
	@Path("versioneMappa")
	public int getVersioneMappa(@QueryParam("nome") String nome){
		if(!OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� "+nome+" non � presente nel server."))
					.build());
		}
		return OntologiaCitta.getVersioneMappa(nome);
	}
	
	//Restituisce la versione attuale dei pesi, relativa alla citt� in argomento
	@RolesAllowed({"utente","admin"})
	@GET
	@Path("versionePesi")
	public int getVersionePesi(@QueryParam("nome") String nome){
		if(!OntologiaCitta.esisteCitta(nome)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� "+nome+" non � presente nel server."))
					.build());
		}
		return OntologiaCitta.getVersionePesi(nome);
	}

}
