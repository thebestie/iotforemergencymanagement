package com.gruppo1;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringEscapeUtils;
import org.glassfish.jersey.internal.util.Base64;
import org.glassfish.jersey.server.ExtendedUriInfo;

import com.gruppo1.model.OntologiaPesi;
import com.gruppo1.model.OntologiaCitta;
import com.gruppo1.model.OntologiaUtenti;

/*Questa classe intercetta tutte le richieste rivolte al server, 
 * con lo scopo di autorizzare solo quelle provenienti da utenti
 * con i permessi necessari ad accedere alla risorsa richiesta*/

@Provider
public class FiltroAutorizzazione implements ContainerRequestFilter {
	
	private Boolean primoAvvio = true;//Variabile utilizzata per caricare i grafi RDF dai file agli oggetti che le manipolano, solo al primo avvio
	private static final String AuthProperty="Authorization";
	private static final String AutSchema="Basic"; 
	private static ExtendedUriInfo uriInfo;
	
	//Questo metodo stabilisce le operazioni da svolgere per ogni richiesta
	@Override
	public void filter(ContainerRequestContext requestContext){
		
		//Le istruzioni commentate vanno decommentate solo se i file di salvataggio non esistono
		if(primoAvvio){
			OntologiaUtenti.setModel();
			//OntologiaUtenti.salvaGrafo();
			OntologiaUtenti.leggiGrafo();
			OntologiaCitta.setModel();
			//OntologiaCitta.salvaGrafo();
			OntologiaCitta.leggiGrafo();
			OntologiaPesi.setModel();
			//OntologiaPesi.salvaGrafo();
			OntologiaPesi.leggiGrafo();
			primoAvvio=false;
		}

		uriInfo = (ExtendedUriInfo) requestContext.getUriInfo();
		Method method=uriInfo.getMatchedResourceMethod().getInvocable().getHandlingMethod();
		
        //Prendo gli header dalla richiesta
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();
         
        //Prendo l'header relativo all'autorizzazione
        final List<String> authorization = headers.get(AuthProperty);
		
        //Il controllo parte solo in assenza della notazione @PermitAll
        if(method.isAnnotationPresent(PermitAll.class)){
        	//non fare nulla
    	//Se l'annotazione � DenyAll si restituisce un errore
        }else if(method.isAnnotationPresent(DenyAll.class)){
        	throw new WebApplicationException(Response 
					.status(Status.UNAUTHORIZED)
					.header("IOT", StringEscapeUtils.escapeHtml4("La risorsa richiesta non � accessibile dall'esterno"))
					.build());
    	//Se mancano le credenziali e sono specificati dei ruoli precisi viene restituito un errore
        }else if(authorization == null || authorization.isEmpty()){
        	throw new WebApplicationException(Response 
					.status(Status.BAD_REQUEST)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono state inoltrate le credenziali dell'utente"))
					.build());
        }else if(method.isAnnotationPresent(RolesAllowed.class)){
             
            //Prendo lo username e la password codificati
            final String encodedUserPassword = authorization.get(0).replaceFirst(AutSchema + " ", "");
             
            //Decodifico
            String usernameAndPassword = null;
            usernameAndPassword = new String(Base64.decodeAsString(encodedUserPassword));
 
            //Divido username e password in token differenti
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();
            
            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
            
            isUserAllowed(username, password, rolesSet, requestContext);
        }
    }
	
	//Questo metodo verifica se le credenziali inoltrate sono corrette e se l'utente dispone dei permessi necessari
	private void isUserAllowed(final String username, final String password, final Set<String> rolesSet,ContainerRequestContext requestContext){
        if(!OntologiaUtenti.esisteUtente(username)){
        	throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("L'utente "+username+" non esiste"))
					.build());
        }else if(!OntologiaUtenti.passwordCorretta(username, password)){
        	throw new WebApplicationException(Response 
					.status(Status.BAD_REQUEST)
					.header("IOT", StringEscapeUtils.escapeHtml4("Password errata"))
					.build());
        }else if(!rolesSet.contains(OntologiaUtenti.getRuolo(username))){
        	throw new WebApplicationException(Response 
					.status(Status.UNAUTHORIZED)
					.header("IOT", StringEscapeUtils.escapeHtml4("L'utente non ha i permessi necessari ad accedere alla risorsa richiesta"))
					.build());
        }	        
        return;
    }

}
