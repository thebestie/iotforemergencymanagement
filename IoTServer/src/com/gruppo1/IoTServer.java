package com.gruppo1;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;

import com.gruppo1.model.OntologiaCitta;
import com.gruppo1.model.OntologiaUtenti;

/*Questa � la classe applicazione dell'applet, inizialmente veniva utilizzata solo per il caricamento dei grafi,
 * poi questa funzione � stata gestita da FiltroAutorizzazione ma si � comunque deciso di lasciare la possibilit�
 * all'amministratore di caricare manualmente i dati*/

@ApplicationPath("/")
@Path("/")
public class IoTServer extends Application {
	
	//Caricamento manuale dei grafi RDF
	@RolesAllowed("admin")
	@GET
	public String inizializza(){
		OntologiaUtenti.setModel();
		//OntologiaUtenti.salvaGrafo();
		OntologiaUtenti.leggiGrafo();
		OntologiaCitta.setModel();
		//OntologiaCitta.salvaGrafo();
		OntologiaCitta.leggiGrafo();
		return "Grafi caricati correttamente";
	}
}
