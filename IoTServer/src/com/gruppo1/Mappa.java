package com.gruppo1;

import java.io.File;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;

import com.gruppo1.model.OntologiaCitta;

/*Questa classe ha lo scopo di permettere il download delle mappe salvate all'interno del server*/

@Path("mappe")
public class Mappa {	
	
	//Download della mappa richiesta in argomento
	@RolesAllowed({"utente","admin"})
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response download(@QueryParam("citta") String citta) {
		File mappa = new File("Mappe/"+citta+".zip");
		if(!mappa.exists()){	    
	    	throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La mappa richiesta non esiste"))
					.build());
		}
	    ResponseBuilder risposta = Response.ok((Object) mappa);
	    risposta.header("Content-Disposition",
	        "attachment; filename="+citta+".zip")
	        .header("Content-Length", mappa.length())
	        .header("Versione", OntologiaCitta.getVersioneMappa(citta));
	    return risposta.build(); 
	}

}
