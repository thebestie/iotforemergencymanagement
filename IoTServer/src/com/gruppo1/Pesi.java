package com.gruppo1;

import java.util.Iterator;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gruppo1.model.OntologiaCitta;
import com.gruppo1.model.OntologiaPesi;

/*Questa classe si occupa dell'inserimento e gestione dei pesi. Da qui l'amministratore pu� inserire nuovi pesi
 * e l'utente pu� manipolare le informazioni relative all'affollamento delle strade*/

@Path("pesi")
public class Pesi {
	
	//Inserimento di un nuovo arco e tutte le relative informazioni (via, citt� ma anche pesi statici e dimensioni)
	@RolesAllowed("admin")
	@POST
	public String inserisciArco(@FormParam("id") int id, @FormParam("via") String via, @FormParam("citta") String citta, @FormParam("v") double v, @FormParam("i") double i, @FormParam("c") double c, @FormParam("corsie") int corsie, @FormParam("larghezza") double larghezza, @FormParam("lunghezza") double lunghezza){
		if(OntologiaPesi.esisteArco(citta, id)){
			throw new WebApplicationException(Response 
					.status(Status.CONFLICT)
					.header("IOT", StringEscapeUtils.escapeHtml4("Un arco con lo stesso ID � gi� presente"))
					.build());
		}
		OntologiaPesi.inserisciArco(id,via,citta,v,i,c,larghezza,lunghezza);
		return "Arco inserito correttamente";
	}
	
	//Questo metodo permette di ottenere tutti i pesi relativi alla citt� in argomento, in formato JSON
	@RolesAllowed({"utente","admin"})
	@GET
	@Produces("application/json")
	public String getPesi(@QueryParam("citta") String citta) throws JSONException{
		if(!OntologiaCitta.esisteCitta(citta)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� "+citta+" non � presente nel server."))
					.build());
		}
		Iterator<Integer> archi=OntologiaPesi.archi(citta).iterator();
		JSONObject principale = new JSONObject();
		principale.put("versione", OntologiaCitta.getVersionePesi(citta));
		JSONArray array = new JSONArray();
		int id;
		while(archi.hasNext()){
			id=archi.next();
			array.put(new JSONObject("{id:"+id+",k:"+OntologiaPesi.peso(citta, id)+"}"));
		}
		principale.put("pesi", array);
		return principale.toString();
	}
	
	//Metodo utilizzato per la modifica delle informazioni relative all'affollamento delle strade
	@RolesAllowed({"utente","admin"})
	@PUT
	public Response aggiornaNumPersone(@FormParam("citta") String citta,@FormParam("idPrec") int idPrec,@FormParam("idSucc") int idSucc){
		if(idPrec!=-1 && OntologiaPesi.esisteArco(citta, idPrec)){
		OntologiaPesi.decrementaNumeroPersone(citta, idPrec);
		}
		if(OntologiaPesi.esisteArco(citta, idSucc)){
		OntologiaPesi.incrementaNumeroPersone(citta, idSucc);
		}
		return Response.ok().build();
	}
	
	//Metodo utilizzato per ottenere i pesi dinamici relativi alla citt� in argomento
	@RolesAllowed({"utente","admin"})
	@GET
	@Path("los")
	@Produces("application/json")
	public String getLos(@QueryParam("citta") String citta) throws JSONException {
		if(!OntologiaCitta.esisteCitta(citta)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� "+citta+" non � presente nel server."))
					.build());
		}
		Iterator<Integer> archi=OntologiaPesi.archi(citta).iterator();
		JSONObject principale = new JSONObject();
		JSONArray array = new JSONArray();
		int id;
		while(archi.hasNext()){
			id=archi.next();
			array.put(new JSONObject("{id:"+id+",los:"+OntologiaPesi.los(citta, id)+"}"));
		}
		principale.put("pesi", array);
		return principale.toString();
	}
	
	//Metodo utilizzato dall'amministratore per azzerare le informazioni relative all'affollamento delle strade
	@RolesAllowed("admin")
	@PUT
	@Path("azzera")
	public String azzeraNumPersone(@FormParam("citta") String citta){
		if(!OntologiaCitta.esisteCitta(citta)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("La citt� "+citta+" non � presente nel server."))
					.build());
		}
		OntologiaPesi.azzeraNumPersone(citta);
		return "Numero persone azzerato";
	}

	
}
