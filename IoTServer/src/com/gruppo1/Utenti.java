package com.gruppo1;


import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;

import com.gruppo1.model.OntologiaUtenti;

/*Questa classe si occupa di gestire la regitrazione di nuovi utenti e di ottenerne il ruolo, se richiesto*/ 

@Path("utenti")
public class Utenti {
	
	//Registrazione di un nuovo utente
	@PermitAll
	@POST
	public String registraUtente(@FormParam("nome") String nome,@FormParam("cognome") String cognome,@FormParam("nick") String nick,@FormParam("pass") String pass){
		if(OntologiaUtenti.esisteUtente(nick)){
			throw new WebApplicationException(Response 
					.status(Status.CONFLICT)
					.header("IOT", StringEscapeUtils.escapeHtml4("Il nickname scelto � gi� in uso"))
					.build());
		}
		OntologiaUtenti.registraUtente(nome, cognome, nick, pass);		
		return "Utente inserito correttamente";
	}
	
	//Ruolo dell'utente in argomento
	@RolesAllowed("admin")
	@GET
	@Path("ruolo")
	public String getRuolo(@QueryParam("nick") String nick){
		if(!OntologiaUtenti.esisteUtente(nick)){
			throw new WebApplicationException(Response 
					.status(Status.NOT_FOUND)
					.header("IOT", StringEscapeUtils.escapeHtml4("L'utente "+nick+" non esiste"))
					.build());
		}
		return OntologiaUtenti.getRuolo(nick);
	}
	
}
