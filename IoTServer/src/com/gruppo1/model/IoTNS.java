package com.gruppo1.model;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

/*Classe utilizzata per la definizione del namespace necessario per creare lo store di triple RDF */

public class IoTNS {
	protected static final String uri ="http://IoTForEmergency/dataset/";
	
	//Restituisce quello che dovrebbe essere l'URI dell'ontologia, una volta che sar� implementata
	public static String getURI() {
        return uri;
  }
	
	private static Model m = ModelFactory.createDefaultModel();
	
	//Utente
	public static final Property Nome = m.createProperty(uri, "nome" );
	public static final Property Cognome = m.createProperty(uri, "cognome" );
	public static final Property Nick = m.createProperty(uri, "nick" );
	public static final Property Pass = m.createProperty(uri, "pass" );
	public static final Property Ruolo = m.createProperty(uri, "ruolo" );
	
	//citta
	public static final Property VersioneMappa = m.createProperty(uri, "versioneMappa" );
	public static final Property VersionePesi = m.createProperty(uri, "versionePesi" );
	
	public static final Property NomeCitta = m.createProperty(uri, "nomeCitta" );
	
	//Pesi
	public static final Property ID = m.createProperty(uri, "id" );
	public static final Property Via = m.createProperty(uri, "via" );
	public static final Property Vulnerabilita = m.createProperty(uri, "v" );
	public static final Property RischiVita = m.createProperty(uri, "i" );
	public static final Property DatiVariabili = m.createProperty(uri, "c" );
	public static final Property NumeroPersone = m.createProperty(uri, "numPersone" );
	public static final Property Larghezza = m.createProperty(uri, "larghezza" );
	public static final Property Lunghezza = m.createProperty(uri, "lunghezza" );
}