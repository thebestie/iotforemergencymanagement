package com.gruppo1.model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.util.FileManager;

/*Classe che si occupa della manipolazione e gestione del file "citta.rdf" relativo all'ontologia delle citt�.
 * Tutte le richieste volte ad alterare il suddetto file passano sempre per questa classe.
 * Ogni richiesta di manipolazione del grafo si conclude sempre con il salvataggio del grafo nel file apposito. */

public class OntologiaCitta {
	
	private final static String ontologia="Ontologie/citta.rdf";
	private static Model model;
	
	//Creazione della variabile di tipo Model per la manipolazione del grafo
	public static void setModel() {
		OntologiaCitta.model=ModelFactory.createDefaultModel();
		model.setNsPrefix( "IoT", IoTNS.uri);
		return;
	}
	
	//Caricamento delle informazioni contenute nel file di salvataggio all'interno dell'oggetto
	public static void leggiGrafo(){
		InputStream in = FileManager.get().open(ontologia);
		if (in == null) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non ho trovato: "+ontologia))
					.build());
        }
		model.read(in, "");
		return;
	}	
	
	//Salvataggio del grafo dall'oggetto al file
	public static void salvaGrafo(){
		FileOutputStream out=null;
		try {
			out = new FileOutputStream(ontologia);
		} catch (FileNotFoundException e1) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono riuscito ad aprire: "+ontologia+e1))
					.build());
		}
        model.write(out);
        try {
			out.close();
		} catch (IOException e) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono riuscito a chiudere lo stream in output su: "+ontologia+e))
					.build());
		}
        return;
	}
	
	//Aggiornamento della versione della mappa relativamente alla citt� in argomento
	public static Boolean aggiornaVersioneMappa (String nomeCitta,int versione){
		Boolean esito=false;
		Resource citta=model.getResource(IoTNS.getURI()+nomeCitta);
		int versAttuale=citta.getRequiredProperty(IoTNS.VersioneMappa).getInt();
		if(versAttuale<versione){
			citta.removeAll(IoTNS.VersioneMappa);
			citta.addLiteral(IoTNS.VersioneMappa, versione);
			salvaGrafo();
			esito=true;
		}
		return esito;
	}
	
	//Aggiornamento della versione dei pesi relativamente alla citt� in argomento
	public static Boolean aggiornaVersionePesi (String nomeCitta,int versione){
		Boolean esito=false;
		Resource citta=model.getResource(IoTNS.getURI()+nomeCitta);
		int versAttuale=citta.getRequiredProperty(IoTNS.VersionePesi).getInt();
		if(versAttuale<versione){
			citta.removeAll(IoTNS.VersionePesi);
			citta.addLiteral(IoTNS.VersionePesi, versione);
			salvaGrafo();
			esito=true;			
		}
		return esito;
	}
	
	//Inserimento di una nuova citt� all'interno del grafo
	public static void inserisciCitta(String nome,int versioneMappa,int versionePesi){
		model.createResource(IoTNS.getURI()+nome)
		.addProperty(IoTNS.NomeCitta, nome)
		.addProperty(IoTNS.VersioneMappa,Integer.toString(versioneMappa))
		.addProperty(IoTNS.VersionePesi,Integer.toString(versionePesi));
		salvaGrafo();
		return;
	}
	
	//Restituzione della versione della mappa
	public static int getVersioneMappa(String nomeCitta){
		return model.getResource(IoTNS.getURI()+nomeCitta).getRequiredProperty(IoTNS.VersioneMappa).getInt();
	}
	
	//Restituzione della versione dei pesi
	public static int getVersionePesi(String nomeCitta){
		return model.getResource(IoTNS.getURI()+nomeCitta).getRequiredProperty(IoTNS.VersionePesi).getInt();

	}
	
	//Restituisce true se la citt� in argomento � presente all'interno del server
	public static boolean esisteCitta(String nome){
		Resource citta=ResourceFactory.createResource(IoTNS.getURI()+nome);
		return model.contains( citta, null, (RDFNode) null );
	}
	
	//Restituisce l'elenco delle citt� presenti nel server
	public static List<String> getCitta(){
		ResIterator elenco=model.listSubjects();
		List<String> citta = new ArrayList<String>();
		while(elenco.hasNext()){
			citta.add(elenco.next().getRequiredProperty(IoTNS.NomeCitta).getString());
		}
		return citta;
	}

}
