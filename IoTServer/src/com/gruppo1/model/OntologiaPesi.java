package com.gruppo1.model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.util.FileManager;

/*Classe che si occupa della manipolazione e gestione del file "pesi.rdf" relativo all'ontologia dei pesi delle citt�.
 * Tutte le richieste volte ad alterare il suddetto file passano sempre per questa classe.
 * Ogni richiesta di manipolazione del grafo si conclude sempre con il salvataggio del grafo nel file apposito. */

public class OntologiaPesi {
	private final static String ontologia="Ontologie/pesi.rdf";
	private static Model model;
	private static final double pV=0.08;
	private static final double pI=0.19;
	private static final double pLOS=0.17;
	private static final double pC=0.53;
	
	public static double getPv() {
		return pV;
	}

	public static double getPi() {
		return pI;
	}

	public static double getPlos() {
		return pLOS;
	}

	public static double getPc() {
		return pC;
	}

	//Creazione della variabile di tipo Model per la manipolazione del grafo
	public static void setModel() {
		OntologiaPesi.model=ModelFactory.createDefaultModel();
		model.setNsPrefix( "IoT", IoTNS.uri);
		return;
	}

	//Caricamento delle informazioni contenute nel file di salvataggio all'interno dell'oggetto
	public static void leggiGrafo(){
		InputStream in = FileManager.get().open(ontologia);
		if (in == null) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non ho trovato: "+ontologia))
					.build());
        }
		model.read(in, "");//Se il file da cui legge � vuoto, restituisce errore
		return;
	}	
	
	//Salvataggio del grafo dall'oggetto al file
	public static void salvaGrafo(){
		FileOutputStream out=null;
		try {
			out = new FileOutputStream(ontologia);
		} catch (FileNotFoundException e1) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono riuscito ad aprire: "+ontologia+e1))
					.build());
		}
        model.write(out);
        try {
			out.close();
		} catch (IOException e) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono riuscito a chiudere lo stream in output su: "+ontologia+e))
					.build());
		}
        return;
	}
	
	//Metodo utilizzato per l'inserimento di un nuovo arco
	public static void inserisciArco(int id, String via, String citta, double v, double i, double c, double larghezza, double lunghezza){
		model.createResource(IoTNS.getURI()+citta+id)
               .addProperty(IoTNS.ID, Integer.toString(id))
               .addProperty(IoTNS.Via,via)
               .addProperty(IoTNS.NomeCitta,citta)
               .addProperty(IoTNS.Vulnerabilita,Double.toString(v))
               .addProperty(IoTNS.RischiVita,Double.toString(i))
               .addProperty(IoTNS.DatiVariabili,Double.toString(c))
               .addProperty(IoTNS.NumeroPersone,"0")
               .addProperty(IoTNS.Larghezza, Double.toString(larghezza))
               .addProperty(IoTNS.Lunghezza, Double.toString(lunghezza));
		salvaGrafo();
		return;
	}
	
	//Restituisce true se l'id inoltrato � relativo ad un arco esistente
	public static boolean esisteArco(String citta, int id ){
		Resource arco=ResourceFactory.createResource(IoTNS.getURI()+citta+id);
		return model.contains( arco, null, (RDFNode) null );
	}
	
	//Metodo utilizzato per l'incremento di 1 del numero di persono presenti sull'arco in argomento
	public static void incrementaNumeroPersone(String citta, int id){
		Resource arco=model.getResource(IoTNS.getURI()+citta+id);
		int vecchio=arco.getRequiredProperty(IoTNS.NumeroPersone).getInt();
		arco.removeAll(IoTNS.NumeroPersone);
		arco.addProperty(IoTNS.NumeroPersone, Integer.toString(vecchio+1));
		salvaGrafo();
		return;
	}
	
	//Metodo utilizzato per il decremento di 1 del numero di persono presenti sull'arco in argomento
	public static void decrementaNumeroPersone(String citta, int id){
		Resource arco=model.getResource(IoTNS.getURI()+citta+id);
		if(arco.getRequiredProperty(IoTNS.NumeroPersone).getInt()>0){
			int vecchio=arco.getRequiredProperty(IoTNS.NumeroPersone).getInt();
			arco.removeAll(IoTNS.NumeroPersone);
			arco.addProperty(IoTNS.NumeroPersone, Integer.toString(vecchio-1));
			salvaGrafo();
		}
		return;
	}
	
	
	//Restituisce il peso statico complessivo dell'arco in argomento
	public static double peso(String citta, int id){
		Resource arco=model.getResource(IoTNS.getURI()+citta+id);
		return arco.getRequiredProperty(IoTNS.Vulnerabilita).getDouble()*pV+arco.getRequiredProperty(IoTNS.RischiVita).getDouble()*pI+arco.getRequiredProperty(IoTNS.DatiVariabili).getDouble()*pC;
	}
	
	//Restituisce il LOS dell'arco in argomento
	public static double los(String citta, int id){
		Resource arco=model.getResource(IoTNS.getURI()+citta+id);
		double los=-1;
		los=((arco.getRequiredProperty(IoTNS.Larghezza).getDouble()*arco.getRequiredProperty(IoTNS.Lunghezza).getDouble())/arco.getRequiredProperty(IoTNS.NumeroPersone).getDouble())*pLOS;
		if(los==-1){
			los=0;
		}else if(los>=3.7){
			los=0;
		}else if(los>=2.2){
			los=0.33;
		}else if(los>=1.4){
			los=0.67;
		}else if(los>=0.75){
			los=1;
		}else{
			los=1000;
		}
		return los;
	}
	
	//Restituisce tutti gli archi di una citt�
	public static List<Integer> archi(String citta){
		ResIterator iter=model.listResourcesWithProperty(IoTNS.NomeCitta, citta);
		List<Integer> edge = new ArrayList<Integer>();
		while(iter.hasNext()){
			edge.add(iter.next().getRequiredProperty(IoTNS.ID).getInt());
		}
		return edge;
	}

	//Metodo utilizzato per l'azzeramento del numero di persone presenti negli archi della citt� in argomento
	public static String azzeraNumPersone(String citta){
		ResIterator archi=model.listResourcesWithProperty(IoTNS.NomeCitta, citta);
		while(archi.hasNext()){
			Resource arco=archi.next();
			arco.removeAll(IoTNS.NumeroPersone);
			arco.addProperty(IoTNS.NumeroPersone, Integer.toString(0));
			salvaGrafo();
		}
		return "Numero persone azzerato con successo";
	}
}
