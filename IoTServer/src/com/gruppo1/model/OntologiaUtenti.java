package com.gruppo1.model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringEscapeUtils;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.util.FileManager;

/*Classe che si occupa della manipolazione e gestione del file "utenti.rdf" relativo all'ontologia degli utenti.
 * Tutte le richieste volte ad alterare il suddetto file passano sempre per questa classe.
 * Ogni richiesta di manipolazione del grafo si conclude sempre con il salvataggio del grafo nel file apposito. */


public final class OntologiaUtenti {
	
	private final static String ontologia="Ontologie/utenti.rdf";
	private static Model model;
	
	//Creazione della variabile di tipo Model per la manipolazione del grafo
	public static void setModel() {
		OntologiaUtenti.model=ModelFactory.createDefaultModel();
		model.setNsPrefix( "IoT", IoTNS.uri);
		return;
	}

	//Restituisce l'ontologia aggiornata con il file di triple rdf. 
	public static void leggiGrafo(){
		InputStream in = FileManager.get().open(ontologia);
		if (in == null) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT",StringEscapeUtils.escapeHtml4("Non ho trovato: "+ontologia))
					.build());
        }
		model.read(in, "");//Se il file da cui legge � vuoto, d� errore
		return;
	}	
	
	//Salva il grafo attuale in un file utenti.rdf
	public static void salvaGrafo(){
		FileOutputStream out=null;
		try {
			out = new FileOutputStream(ontologia);
		} catch (FileNotFoundException e1) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono riuscito ad aprire: "+ontologia+e1))
					.build());
		}
        model.write(out);
        try {
			out.close();
		} catch (IOException e) {
			throw new WebApplicationException(Response 
					.status(Status.INTERNAL_SERVER_ERROR)
					.header("IOT", StringEscapeUtils.escapeHtml4("Non sono riuscito a chiudere lo stream in output su: "+ontologia+e))
					.build());
		}
        return;
	}
	
	//Registra un nuovo utente
	public static void registraUtente(String nome, String cognome, String nick, String pass){	
		model.createResource(IoTNS.getURI()+nick)
               .addProperty(IoTNS.Nome, nome)
               .addProperty(IoTNS.Cognome, cognome) 
               .addProperty(IoTNS.Nick, nick)
               .addProperty(IoTNS.Pass, pass)
               .addProperty(IoTNS.Ruolo, "utente");
		salvaGrafo();
		return;
	}
	
	//Restituisce true se l'utente in argomento esiste. L'id sarebbe il nickname
	public static Boolean esisteUtente(String id){
		Resource utente=ResourceFactory.createResource(IoTNS.getURI()+id);
		return model.contains( utente, null, (RDFNode) null );
	}
	
	//Restituisce true se la password inoltrata corrisponde a quella del nick inoltrato
	public static Boolean passwordCorretta(String nick, String pass){
		Resource utente = model.getResource(IoTNS.getURI()+nick);
		return utente.getRequiredProperty(IoTNS.Pass).getString().equals(pass);
	}
	
	//Restituisce il ruolo dell'utente in argomento
	public static String getRuolo(String nick){
		return model.getResource(IoTNS.getURI()+nick).getRequiredProperty(IoTNS.Ruolo).getString();
	}
}
